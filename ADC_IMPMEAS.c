#include "msp430x24x.h"
#include "intrinsics.h" // Intrinsic functions
#include "stdint.h" // Standard integer types
#include "math.h" // Standard integer types
#include "ADC_IMPMEAS.h"
#include "globals.h"
#include "definitions.h"

unsigned int wdt=0;			
unsigned char start=0;											// Controlls if the start seq. is finished
unsigned char retries=0;										// Help byte to count the nr of retries that took place
unsigned int  ADC_median_result;								// Median result of the ADC and filter function 
unsigned char weatherstation_status=0;							// Byte from motherstation that sets different modes in the roadsensor
unsigned char SEND[20] = {0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20};		// Data to be sent buffer
signed char impedance_data_to_calculate[400];					// Total received "RAW" data of the impedance from AD5933
unsigned char data_register_AD5933;								// Received status register of the AD5933
unsigned char slave_address_AD5933=0x0D;						// Address of the AD5933
unsigned char impedance_data[4]={0x00,0x00,0x00,0x00};			// Temp received "RAW" data of impedanse from AD5933
unsigned char dest_address_high[4]={0x00,0x13,0xA2,0x00};		// Address to the mother node, filled when received start meas command
unsigned char dest_address_low[4]={0x00,0x00,0x00,0x00};		// 
unsigned char send_data[27];									// API message
unsigned char receive[300];										// Received message
unsigned char RXcount;											// Received nr of received messages
unsigned char API_mode=1;										// API mode ON = 1 OFF = 0
unsigned int BatteryLevel=0;									// Battery level of system
unsigned int RoadTemperature;									// Calculated and mean value of the road temperature
float TemperatureMem;
unsigned long destL;
unsigned long destH=0x0013A200;										// Adress for the mothernode in long											
extern unsigned char recDsubBuf[];								// Receive buffer								
extern unsigned int endDsubPnt;									// Last byte in received message
extern unsigned int startDsubPnt;								// Start byte position of message

/**
 * Alternativ huvudslinga, anv�nds n�r enheten uppdaterats remote.  
 */
#pragma CODE_SECTION(mainx,".flashx")
void mainx(void)
{
}

/**
 * Slinga som k�rs n�r remote update genomf�rs
 */
#pragma CODE_SECTION(applicationProgramB,".flashb")
void applicationProgramB (void)
{
}

/**
 * Huvudslinga som vid uppstart initierar systemet, l�ser inst�llningar fr�n processorns flash och sedan loopar en slinga som g�r allt som ska g�ras.
 */
#pragma CODE_SECTION(main,".flasha")
void main(void)
{
	
	unsigned int i=0;
	unsigned char iAck=0;
	unsigned char firstMeas=0;
	WDTCTL = WDTPW+WDTHOLD;                 	// Disable watchdog  , 
  	
  	_EINT();									// Interrupt enabled
 	init();										// Initialize the road sensor
 	//TEST
 	//P1OUT |= 0x02;	
	////
// 	startup_for_pt100();
// 	initiera_AD5933();
// 	impMeasSeq();
	//
	/*ADC12_measurment(1);
					  	P1OUT|=0x01; // Enable 2.048V reference
					  	for(i=1;i<8;i++)
						{
							SEND[i]=0;
						}
					  	ADC12_measurment(1);							// Preformes 19 battery check measurments and stores the mean median value in SEND data		
					  	startup_for_pt100();							// Call for the function to start the PT-100 amplification circuit
					  	ADC12_measurment(0);							// Preformes 19 temp. meas. and stores the mean median value in SEND data
					  				  	
					  	P1OUT&=~0x01; //  Disable 2.048V reference
					  	//Not included if the HW doesn't have a surface sensor//
					  	initiera_AD5933();							// Initialize the AD5933
					  	impMeasSeq();	*/	
	//
	SEND[0]=SENSORTYPE;
	HandleWdt();
	start=1;									// Startup seq.
	retries=0;
	startLedFlash();								// Startsequence for led
	//startLedFlash2();	
	/*do											// Wait for start byte "3E" from mothernode
  	{
  		P5OUT|=0x02;
  		start=receive_XBee(17,0,0x90);				// Waiting for receiving start measurment (0x3E) from mother node
  		if(((start==1) && (retries < 30))||start==0xFF){	
  			retries++;								
  			WdtONOFF(0);							// Put Wdt to hold during LPM4 
  			LPM4;									//Set's the MSP430 in LPM4 consuming approx. 0.1uA waiting for external interrupt from XBee		
  			WdtONOFF(1);							// Start Wdt
  			HandleWdt();
  		}
  		HandleWdt();
  	_NOP();
  	}while((start==1 || start==0xFF)&& retries < 30 );
 _NOP();
 */
  //if(start==0)
  	//{
  		//for(iAck=0;iAck<5;iAck++)				// Maximum of 5 ACKACK retries
  		//{
	  		//if(start_ACK()==0)			// Send ACKACK and check if ACKACK has been received by the mother unit
	  		//{
		  		_NOP();
		  		// MAIN FUNKTION WHILE LOOP//
		  		while(1){
		  			if()

		  		}








		  		while (1)				
		  		{ 
			  			RXcount=0;
					  	_NOP();
				  		//WdtONOFF(0);
			 			//LPM4; 											//Set's the MSP430 in LPM4 consuming approx. 0.1uA waiting for external interrupt from XBee	
			 			//WdtONOFF(1);
			 			//K//Enable MSP on measurement board										  						  	
					  	P1OUT|=0x01; // Enable 2.048V reference
					  	for(i=1;i<8;i++)
						{
							SEND[i]=0;
						}
					  	ADC12_measurment(1);							// Preformes 19 battery check measurments and stores the mean median value in SEND data		
					  	P1OUT&=~0x01; //  Disable 2.048V reference
					  	MeasCardCom('S','2','1');
					  	systemtime=0;
					  	TBCCTL0 |= CCIE; //Enable timer interrupt	
					  	do{
					  		HandleWdt();
					  	}while(systemtime<2000);
					  	TBCCTL0 &= ~CCIE; //Disable timer interrupt	
					  	MeasCardCom('S','4','1');
					  	MeasCardCom('T','2','2');
					  	
						convert_SEND_buffer();
					  	convert_to_API_struct();						// Converts the message to API structure 
					  	
					  	
					  	HandleWdt();
					  	weatherstation_status=receive_XBee(17,3,0x90);	// Receive the status of the weatherstation to see if measured data is requested
					  	if(weatherstation_status==1)					// Check for request measure data 
					  	{	
					  											
					  		while((P2IN & 0x08)==1);					// Wait for Clear To Send from XBee
					  		transmitt_uart(send_data,27);				// Transmitt measure data
					  		if(receive_XBee(11,1,0x8B)!=0xFF)			// Check if message has been received
					  		{
					  			if(firstMeas==0)						// Yellow led for 2sec repr. first message OK, total init. of system complete
					  			{
					  				P5OUT|=0x06;
		  							one_sec_delay();
		  							one_sec_delay();
		  							P5OUT&=~0x06;
					  				firstMeas=1;
					  				_NOP();
					  			}
					  			P5OUT|=0x02;							// Status, red led when meas. data sent OK and data rec. in motherstation
					  			for(i=0;i<3;i++)
					  			{
					  				time_delay(65535);
					  			}
					  			P5OUT&=~0x02;		
					  		}
					
					  	}
					  	else if(weatherstation_status==2)				// Check if remote update status has been sent from the motherunit
					  	{
					  		remoteUpdate();
					  		
					  	}
					  	else if (weatherstation_status==3)			// Reset roadsensor
					  	{
					  		P1OUT|=0x04;							// Reset
					  		for(i=0;i<70;i++)						// Hold XBee reset for 4.6s to wait for the mother node to initialize
					  		{
					  			time_delay(65535);
					  			if(i%30==0)
					  			{
					  				HandleWdt();
					  			}
					  		}
					  		FCTL1 = 0;								// Restart
					  	}
					  	else if (weatherstation_status=='>')		// Used to be able to reset the motherunit without reset the sensor because the motherunit waits for ACKACK from the sensors after a reset when sending >
					  	{
					  		start_ACK();
					  	}
					  	//TBCCTL0 &= ~CCIE;							// Interrupt disable - wdt time	
					  	//M�ste g�ra om i XBeePLUS s� att den bara r�knar ACKACK fr�n noder som inte svarat och inte bara antal ACKACK som den f�r in
					  /*	else if (weatherstation_status==0x3E)
					  	{
					  		start_ACK();
					  	*/
					}
					  	
					  	_NOP();
			 	//}
			 	//WdtONOFF(0);
			 	//LPM4;
			 	//WdtONOFF(1);
	  		//}
	  		
	//FCTL1 = 0;	//Restart if not ACKACK received by the motherunit after 5 tries
	//}
	//FCTL1 = 0;	//Restart if Start measurment '>' from mothernode hasn't appeard adter 30 tries
 }

void MeasCardCom(unsigned char type, unsigned char n_wires, unsigned char port){
	P1OUT |= 0x02;											// Start the LTC3200-5.
					  	ADC12_measurment(1); //Battery measurement
					  	TBCCTL0 = CCIE;						// Interrupt enable - systemtime to be able to use the while loops as wait functions for the HW-I2C
	  					HandleWdt();//Init systemtime
					  	startI2C();
					  	
					  	//K//startup_for_pt100();							// Call for the function to start the PT-100 amplification circuit
					  	//K//ADC12_measurment(0);							// Preformes 19 temp. meas. and stores the mean median value in SEND data
					  	systemtime=0;
					  	while((!i2cInitOk) && systemtime<200)//Wait for communication or Error
  						{
  							HandleWdt(); 
  						}
  						if(systemtime>=200) //Error
  						{
  							//TODO?
  							_NOP();
  						}
  						//sendI2CData('Q','S','2','1',0x11); //Query data from Salt sensor on channel 1 with two wires. more data
  						sendI2CData('Q',type,n_wires,port,0x10);
  						//sendI2CData('Q','T','2','2',0x00); //Query data from Temp sensor on channel 2 with two wires. No more data
  						//sendI2CData('Q','T','4','3',0,0); //Query data from Temp sensor on channel 3 with four wires.No more data
  						systemtime=0;
				  		while((!i2cDataReceived) && systemtime<1000) //Wait for data
				  		{
				  			HandleWdt(); 
				  		}
  						if(systemtime>=1000) //Error
  						{
  							//TODO?
  						}				  		
				    	_NOP(); //Breakpoint here
				    	stopI2C(); //Stop communication
				    	systemtime=0;
				    	while((!i2cStopOk)&&systemtime<200) //Wait for data
				  		{
				  			HandleWdt(); 
				  		}
  						if(systemtime>=200) //Error
  						{
  							//TODO?
  						}
  						//K//Disable MSP on measurement board
  						//P3REN&=~0x06;						// Pull up I2C disable
  						//P3OUT&=~0x06;
  						TBCCTL0 &= ~CCIE; //Disable timer interrupt				  		
				  		_NOP();			  	
					  	

					  	//Not included if the HW doesn't have a surface sensor//
					  	//K//initiera_AD5933();							// Initialize the AD5933
					  	//K//impMeasSeq();				  				  	 	
					  	////////////////////////////////////////////////////////
						//Put values into SEND
						shdnMeasCard();
						_NOP();
}

//Pin interrupt for exit Low Power Mode when the measuring cycle interval has been reached.
//The XBee module sets ON/SLEEP high and that triggers the pin interrupt on MSP430 that wakes up from LPM4

#pragma vector = PORT2_VECTOR			
__interrupt void PORT2_ISR(void)
{
  do 
  	{
		P2IFG = 0; 							// Clear interrupt on P2
	}while ((P2IFG != 0));
     
	LPM4_EXIT;			// Exit LPM4 to start the CPU
}

// Disables the TIMERA0 interrut
#pragma vector = TIMERA0_VECTOR
__interrupt void TIMERA0(void)
{
_NOP();	
}
// Disables the TIMERA1 interrut
#pragma vector = TIMERA1_VECTOR
__interrupt void TIMERA1(void)
{
_NOP();	
}

#pragma vector = TIMERB0_VECTOR
__interrupt void TIMERB0(void)
{
	/*if(systemtime%200==0)
	{
		TempClk++;
	}*/
	TBCCR0 = 5000;
	systemtime++;
	if(systemtime%10) //50ms
	{
		i2cHandler();
	}	
	if (systemtime == 60000) 
	{
		systemtime = 0;
	}
	
	TBCCTL0 &= ~CCIFG;		// Reset timer
}

 
#pragma vector=NMI_VECTOR
__interrupt void nmi (void)
{
  IFG1 &= ~NMIIFG;                          // Reclear NMI flag in case bounce
  IE1 |= NMIIE;                             // Enable NMI
}










  
 

 

    
  


