#include "msp430x24x.h"
#include "intrinsics.h" // Intrinsic functions
#include "stdint.h" // Standard integer types
#include "math.h" // Standard integer types
#include "globals.h"
#include "functions.h"
#include "definitions.h"
#include <string.h>


#pragma vector=USCIAB0RX_VECTOR						// Receive data interrupt handler
__interrupt void USCI0RX_ISR(void)
{
	if(UCB0STAT&UCNACKIFG) //Nack interrupt
	{
		UCB0STAT &= ~(UCNACKIFG);       // Clear interrupt flags
		UCB0CTL1 |= UCTXSTP; // generate Stop
	}
	else
	{
	  	while ((!(IFG2&UCA0RXIFG))&& wdt<=18000);               
	  	recDsubBuf[endDsubPnt++] = UCA0RXBUF;           	// Write data from RX buffer 
	                   				
	  	if(recDsubBuf[0]!=0x7E && API_mode==1)  			// Check message start byte from XBee if device in API mode
	  	{
	  		flushRecDsubBuf();
	  	}
	}
}
void startLedFlash()		  // Flash 	yellow when startup						
{
	unsigned int i=0;
	unsigned int j=0;
	for(i=0;i<5;i++)
	{
		P5OUT|=0x06;
		for(j=0;j<100;j++)
		{
			time_delay(1000);
		}
		P5OUT&=~0x06;
		
		for(j=0;j<100;j++)
		{
			time_delay(1000);
		}
		HandleWdt();
	}
	P5OUT=0x00;
}
void startLedFlash2()		  // Flash 	yellow when startup						
{
	unsigned int i=0;
	unsigned int j=0;
	for(i=0;i<5;i++)
	{
		P5OUT|=0x02;
		for(j=0;j<100;j++)
		{
			time_delay(1000);
		}
		P5OUT&=~0x02;
		
		for(j=0;j<100;j++)
		{
			time_delay(1000);
		}
		HandleWdt();
	}
	P5OUT=0x00;
}

void RoadTempCalc()
{
	float res=0;
	float Verefplus=2.048;				// Positiv reference to the ADC
	float Vref=-4.096;					// Negativ reference to bias the instumentation amplifier
	float instr_gain=50.4;				// Gain in the instrumentation amplifier
	float temp_temp=0;
  
		res=(((((float)RoadTemperature*Verefplus)/4096)-Vref)/(0.001*instr_gain));  // Calculate the resistanse in the Pt-100 sensor according to the ADC value 
		if(res<=100)
		{
			temp_temp=((res-100)/0.3935);	// 0.3935 is the linearized value for resistans smaller than 100 ohm
		}
		else
		{
			temp_temp=((res-100)/0.388);	// 0.388 is the linearized value for resistans larger than 100 ohm
		}
		
		// Calibration data added to the calculated temperature
		if(CALIB==1){
			
		if(temp_temp<=0)
		{
				temp_temp=(temp_temp+((float)0.0216*temp_temp-(float)BelowZeroOffset/100))*10;
			}
			else
			{
				temp_temp=(temp_temp+((float)0.0216*temp_temp-(float)AboveZeroOffset/100))*10;
			}
		}
		else{ temp_temp=temp_temp*10;}
		
		TemperatureMem=temp_temp;
		temp_temp=floor(temp_temp+0.5);			// Round off the temp. value
		
        RoadTemperature=(signed int)temp_temp;	// Scale down the temperature to int
        
        SEND[4]= (RoadTemperature & 0xFF00)>>8;
  		SEND[5]= (RoadTemperature & 0x00FF);      
}

/*unsigned long bubbleSort(unsigned long array[], unsigned char nrValues)
{
	unsigned long tempimpmeas;
	unsigned char k=0;
	unsigned char j=0;

	  	for (k=(nrValues-1); k>0 ;k--)	//Bubbel sort to be able to use the median value of the imp. measurment. This will ensure that no faulty data is sent due to tier infulence on the measurment
			  	{
			    	for (j=1; j<=k; j++)
			    	{
			      		if (array[j-1] > array[j])
			      		{
			        		tempimpmeas = array[j-1];
			        		array[j-1] = array[j];
			        		array[j] = tempimpmeas;
			      		}
			    	}
			  	}
			  	return array[2];
}
 */
 // Receives data from the UART by setting a maximum wait time and then waiting for the number of bytes that will be received
// according to the function call. The receive function loops dtime times with a delay of 6.5ms for every loop. The receiving
// function uses an interrupt from the UART to act when a byte is received to the UART. It stores the bytes from the UART in the receive array.
unsigned char receive_UART(unsigned char bytes, unsigned int dtime)
{
	
	unsigned int i=0;
	if(bytes==0xFF){
		IE2 |= UCA0RXIE;					//Enable receive interrupt
		flushRecDsubBuf();
		do{
			_NOP();
			if(endDsubPnt>0){
				if(recDsubBuf[endDsubPnt-1]==0x0A)			// Check if received nr bytes = expected # bytes
				{
					_NOP();
					IE2 &= ~UCA0RXIE;			//Disable receive interrupt
					return 1;

				}
			}
			i++;							// Count up nr of delay time
			time_delay(16000);
			if(i%200==0)
			{
				HandleWdt();
			}
		}while(i<dtime);
		if(i>=dtime)								// Error if receive time elapsed
		{
			IE2 &= ~UCA0RXIE;						// Disable receive interrupt
			return 0;								// Return error
		}
		_NOP();
		return 1;	
		
	}
	else{
		IE2 |= UCA0RXIE;					//Enable receive interrupt
		flushRecDsubBuf();
		do{
			_NOP();
			if(endDsubPnt>=(bytes))			// Check if received nr bytes = expected # bytes
			{
				_NOP();
				IE2 &= ~UCA0RXIE;			//Disable receive interrupt
				
				break;
			}
			i++;							// Count up nr of delay time
			time_delay(6500);
			if(i%200==0)
			{
				HandleWdt();
			}
		}while(i<dtime);
		
		if(i>=dtime)								// Error if receive time elapsed
		{
			IE2 &= ~UCA0RXIE;						// Disable receive interrupt
			return 0;								// Return error
		}
		_NOP();
		return 1;									// Return UART receive OK
	}
	
	
}

void startup_for_pt100(void)
{
	unsigned int start_up_time_PT100 = 8000;				// Startup time for the Pt-100 circuit LTC3200-5 =1.2ms + Vref 4.5 = 200us + additional 100us;	
	SEND[8]=CurrentFirmwareVersion;
	P1OUT |= 0x02;											// Start the LTC3200-5. Main supplie for the PT-100 amplifier circuit
	time_delay(start_up_time_PT100);						// 10ms delay
	
}
// Ad conversion for temp. meas and battery measurment. 

void ADC12_measurment(unsigned char AD_mode)
{
	char number_of_conversions=19;					//Number of measurments
	unsigned int  ADC_results[19];
	unsigned int MeanMediTemp=0;
	
	int temp;
	int i ;
	int j ;	
	unsigned int k ;

	time_delay(65535);
	ADC12CTL0 = ADC12ON | SHT0_15|  MSC;			//Start ADC12 | 1024 SMCLK sample time => 1024us | Multi conversation mode
	ADC12CTL1 = ADC12SSEL_3 | SHP | CONSEQ_2 ;		// Select ADC12 clock source SMCLK | SAMPCON signal is sourced from the sampling timer | Repeat singel channel conversion
	
	if(AD_mode==0)									// Temp meas mode
	{
		ADC12MCTL0 = SREF_2+INCH_0;                 // Vr+ = VeREF+ (external) Vr- = Vss and input ADC 0
		P1OUT|=0x01;								// Enable 2.048V Reference
		time_delay(5000);
	}
	else if(AD_mode==1)								// Battery check mode
	{
		P1OUT|=0x01;								// Enable 2.048V Reference
		ADC12MCTL0 = SREF_2+INCH_4;                 // Vr+ = VeREF+ (external) Vr- = Vss and input ADC 1
		P4OUT |= 0x02;								// Enabel battery check
		time_delay(10000);
		HandleWdt();
	}
	ADC12CTL0 |= ENC;								// Enabling ADC12 conversion	
	ADC_median_result=0;
	
	ADC12CTL0 |= ADC12SC; 							// Starting the ADC12 conversion
	temp=0;
		
	for(i=0; i<number_of_conversions; i++)
	{		
		 time_delay(1000);	
	
		for(k=0;k<50000;k++)
		{
			if((ADC12IFG & BIT0)==1)				// Check if conversion finished
			{
				break;
			}
			else
			{
				time_delay(15);
			}
		}
		//while (((ADC12IFG & BIT0)==0));				// Wait for interrupt flag of ADC12 channel 0 to be set that notifies that a new ADC value is in the buffer
		HandleWdt();
		ADC_results[i] = ADC12MEM0;					// Write result of ADC12 to memory
	}
	ADC12CTL0 &= ~ENC;								// Disable the ADC12 conversion after number_of_conversions sequential conversions	
	
	for (i=(number_of_conversions-1);i>0 ;i--)		//Bubbel sort to be able to use the median value of the temp. measurment. This will ensure that no faulty data is sent
  	{
    	for (j=1; j<=i; j++)
    	{
      		if (ADC_results[j-1] > ADC_results[j])
      		{
        		temp = ADC_results[j-1];
        		ADC_results[j-1] = ADC_results[j];
        		ADC_results[j] = temp;
      		}
    	}
  	}
  	MeanMediTemp=floor((ADC_results[7]+ADC_results[8]+ADC_results[9]+ADC_results[10]+ADC_results[11])/5);
  	ADC_median_result = MeanMediTemp;
  	if(AD_mode==0)			// Temperature data stored in send array
  	{
  		//Store temperatur measurment in send data
  		RoadTemperature=ADC_median_result;
  		RoadTempCalc();
  		
  	}
  	else if(AD_mode==1)		// Battery check data stored in send array
  	{
  		
  		//P6OUT &= ~0x20; //test
  		SEND[6]= (ADC_median_result & 0xFF00)>>8;
  		SEND[7]= (ADC_median_result & 0x00FF);
  		BatteryLevel=ADC_median_result;
  	}
 
  	if(AD_mode==0)									// Temp meas mode
	{
		//P1OUT&=~0x01;								// Disable 2.048V Reference
	}
	else if(AD_mode==1)								// Battery check mode
	{
		//P1OUT&=~0x01;								// Disable 2.048V Reference
		P4OUT &= ~0x02;								// Disable battery check
	}
	_NOP();	 
		
}

void time_delay(unsigned int delaytime_us)
{
//_DINT();
	TACCR0 = delaytime_us;					// Set the value Timer_A will count up to in us
	TACTL = MC_1|ID_0|TASSEL_2|TACLR;		// Set up the Timer_A register: Timer count up to TACCR0|Divide clock by 1 for 1us resolution|SMCLK clock|Clear TAR 
	while((TACCTL0 & CCIFG) == 0);			// Wait for the timer to reach the TACCR0 value 
	TACCTL0 |= ~CCIFG;						// Reset the interrupt overflow flag
	TACCTL0 = MC_0;							// Stop Timer_A	
//_EINT();
}

//Init the AD5933 with freq values, freq hop. etc.
void initiera_AD5933()
{
	unsigned char block_write_data[12];
	unsigned char control_data_1[2] = {0x80, 0xB1}; //B1
	unsigned char control_data_2[2] = {0x81, 0x00};
	unsigned char pointer_data[2] = {0xB0, 0x82};
	
	
	unsigned char reset[2]={0x81,0x10};
	unsigned char s_freq[3]={0x22,0x2E,0xA2};//{0xFF,0xFF,0xFF};//{0x22,0x2E,0xA2};		// Data for start freq 70kHz
	unsigned char inc_freq[3]={0x00,0x00,0x21};		// Data for increment freq
	unsigned char inc[2]={0x00,0x01};				// Number of increments
	unsigned char settlingtime[2]={0x00,0x0F};		// Settling time
	//P6OUT|=0x20;						// Enable GND for imp.sensor
	// Settings in AD5933
	block_write_data[0] = 0xA0;
	block_write_data[1] = 0x0A;
	block_write_data[2] = s_freq[0];
	block_write_data[3] = s_freq[1];
	block_write_data[4] = s_freq[2];
	block_write_data[5] = inc_freq[0];
	block_write_data[6] = inc_freq[1];
	block_write_data[7] = inc_freq[2];
	block_write_data[8] = inc[0];
	block_write_data[9] = inc[1];
	block_write_data[10] = settlingtime[0];
	block_write_data[11] = settlingtime[1];
	
	transmitt_data(control_data_1,2);			//Send stand by mode, 2Vp-p to AD5933	
	transmitt_data(control_data_2,2);			//Send internal clock to AD5933	
	transmitt_data(pointer_data,2);				//Send address pointer where block write will start
	transmitt_data(block_write_data,12); 		//Send block write data
	transmitt_data(reset,2);					//Reset the measuring sequence to ensure the start of the sequence is right
}

// Measuring sequence for the impedance sensor
unsigned char measuring_sequence()
{
	unsigned char control_data_init_start_freq[2] = {0x80, 0x11};   // Command to init freq
	unsigned char control_data_start_freq_sweep[2] = {0x80, 0x21};	// Command to start freq swep
	unsigned char control_data_increment_freq[2] = {0x80, 0x31};	// Command to increment freq
	//unsigned char control_data_repeat_freq[2] = {0x80, 0x41};

	unsigned char stand_by[2] = {0x80, 0xB1};						// Command stand by
	unsigned char mask_freq_sweep_compl = 0x04;						// Mask for freq sweep complete
	unsigned char mask_data_ready = 0x02;							// Mask for data ready
	unsigned char status_register =0x8F;							// Status register address

	
	unsigned int n=0;
	unsigned int i=0;
	unsigned int j=0;
	
	transmitt_data(control_data_init_start_freq,2);			// Initialize the start freq in AD5933
	time_delay(100);										// 0.1ms delay for init. delay
	transmitt_data(control_data_start_freq_sweep,2);		// Start the freq sweep in AD5933

	_NOP();
	for(i=0;i<400;i++)
	{
		impedance_data_to_calculate[i]=0;
	}
	
	while((status_register_poll(status_register,mask_freq_sweep_compl)==0)&& wdt<=18000)	// Loop while freq sweep is not complete
	{
		for(j=0;j<1000;j++)
		{
			if(status_register_poll(status_register,mask_data_ready)==1)	// Check if data of one freq measure is ready
			{
				_NOP();
				break;	
			}
			time_delay(10);
		}
		if(j==1000)
		{
			return 0xFF;			// Return 0xFF if error
		}
		else
		{
			receive_data(0x94,4);	// Receive 4 bytes of data from register starting at register address 0x94 
			for(i=0;i<4;i++)
			{
				impedance_data_to_calculate[4*n+i]=impedance_data[i];	
			}
			transmitt_data(control_data_increment_freq,2);		// Increment the freq. sweep
			n++;
		}
	}
	transmitt_data(stand_by,2);									// Put the AD5933 in standby mode
	n=0;
	return 0x00;
}
	

/*Enabling the transmitting interrupt and the initialization of the transmitting funktion for data to the AD5933*/

unsigned int status_register_poll(unsigned char status_register, unsigned char poll_mask) // Poll specific register for certain byte
{
	receive_data(status_register, 1);
	if((data_register_AD5933&poll_mask))
	return 1;
	else
	return 0;
	
}

// Transmitt data with the I2C bus to the AD5933	
void transmitt_data(unsigned char data[], unsigned char number_of_bytes)
{
	unsigned int i=0;
	unsigned char databytes_to_transmitt;
	
		UCB0I2CSA = slave_address_AD5933;                	// Set initial slave address
	  	UCB0CTL1 |= UCTR;  
	  	//IE2 |= UCB0TXIE;                          		// Enable TX interrupt
	  	UCB0CTL1 &= ~UCSWRST;                     			// Clear SW reset
	  	
	  	databytes_to_transmitt=number_of_bytes;
	  	UCB0STAT=0x00;
	  	TBCCTL0 = CCIE;						// Interrupt enable - systemtime to be able to use the while loops as wait functions for the HW-I2C
	  	HandleWdt();
	  	systemtime=0;
		while ((UCB0CTL1 & UCTXSTP)&& systemtime<=200);             			// Ensure stop condition got sent
	   
	    UCB0CTL1 |=UCTXSTT;														// Start condition
	    systemtime=0;
	    while(((IFG2 & UCB0TXIFG)==0)&& systemtime<=200);						// Wait for no interrupt pending
	 	UCB0TXBUF=data[0];														// First byte to out buffer													
	 	HandleWdt();
	 	systemtime=0;
	    while ((UCB0CTL1 & UCTXSTT)&& systemtime<=200); 						// Start condition sent
	
	    if (( UCB0STAT & UCNACKIFG) != 0) 			// Check if address is NACK
	    { 
			UCB0CTL1 |= UCTXSTP;					//Send stop condition
	    }
	    else
	    {
	    	for(i=0;i<databytes_to_transmitt-1;i++)
	    	{
	    		HandleWdt();
	    		systemtime=0;
	    		while (((IFG2 & UCB0TXIFG) == 0)&& systemtime<=200);  		// Mask interrupt for data to be transmitted
	     		UCB0TXBUF = data[i+1];
	     		systemtime=0;
	     		while(((IFG2 & UCB0TXIFG)==0)&& systemtime<=200);			// Data buffer empty to be able to send new data
	     		if(i==(databytes_to_transmitt-2))		// Check if second last byte to transmitt
	     		{
	     			HandleWdt();
	     			UCB0CTL1 |= UCTXSTP;				// Send Stop condition
	    			IFG2 &= ~UCB0TXIFG;					// Reset interrupt pending
	     		}
	     		HandleWdt();
	    	}
		    for(i=0;i<10;i++)
		    {   
		    	systemtime=0;          
		    	while ((UCB0CTL1 & UCTXSTP)&& systemtime<=200);             // Ensure stop condition got sent                
		 		if(systemtime<200)
		 		{
		 			HandleWdt();
		 			break;
		 		}
		 		HandleWdt();
		    }
	 	
	 	}
	 	TBCCTL0 &=~CCIE;						// Interrupt disable - systemtime
	 _NOP();
}

// Receive data funciton or the I2C interface
void receive_data(unsigned char register_address, unsigned char number_of_bytes)
{
	unsigned char databytes_to_receive;
	unsigned char control[2];
	unsigned char address_pointer[2];
	unsigned int i;
	
	control[0] = 0xA1;
	control[1] = number_of_bytes;
	
	address_pointer[0]=0xB0;
	address_pointer[1]=register_address;
	
	UCB0I2CSA = slave_address_AD5933;           // Set initial slave address
  	UCB0CTL1 &= ~UCSWRST;                     	// Clear SW reset
  	TBCCTL0 = CCIE;								// Interrupt enable - systemtime to be able to use the while loops as wait functions for the HW-I2C
  	systemtime=0;
  	databytes_to_receive=number_of_bytes;
  	
  	transmitt_data(address_pointer, 2); 		// Point at the starting address in the register where the reading will start  	
  	
  	if(number_of_bytes > 1)						//Block read 
  	{
  		
  		while ((UCB0CTL1 & UCTXSTP)&& systemtime <=200);             // Ensure stop condition got sent
   		systemtime=0;
   		UCB0CTL1 |=UCTR;
   		UCB0CTL1 |=UCTXSTT;
    
    	while(((IFG2 & UCB0TXIFG)==0)&& systemtime <=200);			// No interrupt pending, buffer empty
 		systemtime=0;
 		UCB0TXBUF=control[0];										// First byte to be transfered (Block read) 
    	while (( UCB0CTL1 & UCTXSTT)&& systemtime <=200); 			// Wait for start condition to get sent
		systemtime=0;
		
		 if (( UCB0STAT & UCNACKIFG) != 0) 			// Check if address is NACK
    	{ 
			UCB0CTL1 |= UCTXSTP;					//Send stop condition if address not ACK
    	}
    	else
    	{
    		while (((IFG2 & UCB0TXIFG) == 0)&& systemtime <=200);  		// Mask interrupt for data to be transmitted
     		systemtime=0;
     		UCB0TXBUF = control[1];					// Number of bytes to read
			UCB0CTL1 &= ~UCTR;  					// Receive mode
    		UCB0CTL1 |= UCTXSTT;             		// I2C RX-mode, send start condition for pointer to block read
    		IFG2 &= ~UCB0TXIFG;
    	}
          	 //while((IFG2 & UCB0TXIFG)==0);
 			 
    		 while(( UCB0CTL1 & UCTXSTT)&& systemtime <=200); 			// Wait for start condition to get sent
          	systemtime=0;
    		if (( UCB0STAT & UCNACKIFG) != 0) 		// Check if address is NACK
    		{ 
				UCB0CTL1 |= UCTXSTP;					//Send stop condition
    		}
    		else
    		{
    			for(i=0;i<databytes_to_receive-1;i++)
    			{
     				while (((IFG2 & UCB0RXIFG) == 0)&& systemtime <=200);    	// Wait for	data to be received		
   					systemtime=0;
   					impedance_data[i]=UCB0RXBUF;			// Store Real and img. data from AD5933	
    			}
    			UCB0CTL1 |= UCTXSTP; 						// Send Stop condition after 3:e byte
     			while (((IFG2 & UCB0RXIFG) == 0)&& systemtime <=200);			// Wait for last byte to be received
     			systemtime=0;
     			impedance_data[3]=UCB0RXBUF;
    		}                        
    while ((UCB0CTL1 & UCTXSTP)&& systemtime <=200);             			// Ensure stop condition got sent                 	// I2C stop condition
  	systemtime=0;
  	}
  	
    else										// Single byte read
    {
    	while ((UCB0CTL1 & UCTXSTP)&& systemtime <=200);             // Ensure stop condition got sent
    	systemtime=0;
    	UCB0CTL1 &= ~UCTR;						// Receive mode
    	UCB0CTL1 |= UCTXSTT;             		// I2C RX-mode
    						
    	while ((( UCB0CTL1 & UCTXSTT) != 0)&& systemtime <=200);	// Wait for address to be sent and ACK
    	systemtime=0;
    	if (( UCB0STAT & UCNACKIFG) != 0) 		// Check if address is NACK
    	{ 
			UCB0CTL1 |= UCTXSTP;					//Send stop condition
    		data_register_AD5933=0x00;
    	}
    	else
    	{
    		UCB0CTL1 |= UCTXSTP;					//Send stop condition
    		while (((IFG2 & UCB0RXIFG) == 0)&& systemtime <=200);
    		systemtime=0;
    		data_register_AD5933=UCB0RXBUF;
    	}
    		
    	
	}
}

// Calculates the imp. value from the Raw data that AD5933 measured
unsigned long imp_calculation()
{
	signed int real_tot;
	signed int img_tot;
	float abs_value;
	long impedance_value;
	unsigned tempint=0;
	unsigned long Gf=567360584;  //567360584;
	
	if(impedance_data_to_calculate[0]==0 & impedance_data_to_calculate[1]==0 & impedance_data_to_calculate[2]==0 & impedance_data_to_calculate[3]==0)
	{
		return 0xFFFFFFFF;
	}
	else
	{
		// Calculate the impedance value
		real_tot = ((impedance_data_to_calculate[0]<<8) & 0xFF00);
		real_tot |= impedance_data_to_calculate[1] &0xFF;
		
		img_tot = ((impedance_data_to_calculate[2] << 8 )& 0xFF00);
		img_tot |= impedance_data_to_calculate[3] & 0xFF;
		
		abs_value = (sqrt(((float)real_tot*(float)real_tot)+((float)img_tot*(float)img_tot))); // Calculate absolute value of real and imag
		impedance_value=(1*(float)Gf/(abs_value))*10;
	}	
		tempint=impedance_value>>8;
		return impedance_value;
}

// Converts the meas. data to API structure to be able to send the data with the XBee in API mode
void convert_to_API_struct()
{
	unsigned int p=0;
	unsigned int temp=0;
	
	send_data[0]=0x7E;
	send_data[1]=0x00;
	send_data[2]=0x17;
	send_data[3]=0x10;
	send_data[4]=0x01;
	for(p=0;p<4;p++)
	{
		send_data[5+p]=dest_address_high[p];		// Set dest address to the API message
		send_data[9+p]=dest_address_low[p];
	}
	send_data[13]=0xFF;
	send_data[14]=0xFE;
	send_data[15]=0x00;
	send_data[16]=0x00;
	for(p=0;p<9;p++)					// Shift in the measured values to the message
	{
		send_data[17+p]=SEND[p];
	}
	
	for(p=3;p<26;p++)					// Calculate the checksum
	{
		temp=temp+send_data[p];
	}
	temp=0xFF-(temp & 0x00FF);
	
	send_data[26]=temp;
	_NOP();
}

// Receive data that has ben sent from motherunit via the XBee unit 
unsigned char receive_XBee(unsigned char bytes,unsigned char type, unsigned char frameType)
{
	unsigned char TempdestAddressLow[4]={0x00,0x00,0x00,0x00};
	unsigned long TempdestL=0;
	unsigned long i=0; 
	unsigned int j=0;
	unsigned int k=0;
	RXcount=0;
	for(i=0;i<50;i++)
  		{
  			receive[i]=0;
  		}
	for(i=0;i<20000;i++)
	{
		k=0;
		j=0;
		while(((P2IN&0x08)!=0))
		{
			if(j%65000){
				HandleWdt();
				k++;}
				if(k==5000){
					return 0xFF;}
				j++;
		}
		_NOP();
  		if((IFG2&UCA0RXIFG))                			// USCI_A0 RX buffer ready?
  		{
  			receive[RXcount] = UCA0RXBUF;           	// Receives the data from mother unit	
  			RXcount++;
  			if(receive[0]!=0x7E && API_mode==1)  		// Check message start byte from XBee
  			{
  				RXcount=0;
  			}
  			
  			if(RXcount==0x04&&frameType!=0xFF)							// Check that the correct message that is predicited is received
  			{
  				if(receive[3]!=frameType)
  				{
  					RXcount=0;
  				}
  			}
  			
  		}
 		
  		if(RXcount==bytes)								// Receive of transmitt respons finished
  		{
  			_NOP();	
  			break;
  		}
  		time_delay(90); 		
		if(i%3000==0)
 		{
 			HandleWdt();
 		}
	}
	if(i>=20000)
	{
		return 0xFF;
	}
	// Six different receive modes 
	else
	{
		if(type==6){
			if(receive[7]==0x00){
				OperatingSleeptime=receive[11];
				OperatingSleeptime|=((unsigned long)receive[10])<<8;
				OperatingSleeptime|=((unsigned long)receive[9])<<16;
				OperatingSleeptime|=((unsigned long)receive[8])<<24;
				return 1;
			}
			else{
				return 0;
			}
		}
		if(type==5)
		{
			return 1;				// Transparent mode
		}
		if(type==4)					// HW reset from XBee
		{
			return 1;
		}
		if(type==3)					// Weather station status
		{
			for(i=0;i<4;i++)		// Check if the mothernode address has changed, change the lower address segment
				{
					TempdestAddressLow[i]=receive[8+i];
				}
				TempdestL=((unsigned long)(TempdestAddressLow[0]))<<24;		
				TempdestL|=((unsigned long)(TempdestAddressLow[1]))<<16;
				TempdestL|=((unsigned long)(TempdestAddressLow[2]))<<8;
				TempdestL|=((unsigned long)(TempdestAddressLow[3]));

			if(destL!=TempdestL)			// Change dest address if the mother node is changed
			{
				for(i=0;i<4;i++)
				{
					dest_address_low[i]=TempdestAddressLow[i];
				}
				destL=TempdestL;
			}
			return receive[15];
			
		}
		if(type==2)					// AT-command respons
		{
			if(receive[7]==0)
			{
				return 0x00;
			}
			else
			{
				return 0xFF;
			}
		}
		if(type==1)					// Transmitt respons
		{
			if(receive[8]==0)
			{
				return 0x00;
			}
			else
			{
				return 0xFF;
			}
		}
		if(type==0)					// Start ('>') received from main station, Only used during init process
		{
			HandleWdt();
			if(receive[15]=='>'||receive[15]==0x01)
			{
				for(i=0;i<4;i++)
				{
					dest_address_low[i]=receive[8+i];
				}
				// Receive the addr. of the motherunit and store it in memory
				destL=((unsigned long)(dest_address_low[0]))<<24;		
				destL|=((unsigned long)(dest_address_low[1]))<<16;
				destL|=((unsigned long)(dest_address_low[2]))<<8;
				destL|=((unsigned long)(dest_address_low[3]));
				destH=((unsigned long)(dest_address_high[0]))<<24;
				destH|=((unsigned long)(dest_address_high[1]))<<16;
				destH|=((unsigned long)(dest_address_high[2]))<<8;
				destH|=((unsigned long)(dest_address_high[3]));
				HandleWdt();
				AT_prog((unsigned long)(destH),DESTHIGH);
				AT_prog((unsigned long)(destL),DESTLOW);
				return 0x00;
				}
			else{
			return 0x01;}
		}
		_NOP();
	}
}

// Transmitt data to the XBee 
void transmitt_uart(unsigned char data[],unsigned char nr_bytes)		// Transmitt the data to the XBee
{
	unsigned int i=0;
	unsigned int j=0;
		for(i=0;i<nr_bytes;i++)
  		{	
  			HandleWdt();	
  			for(j=0;j<50000;j++)							// Loop while TX-buffer not empty
  			{
  				if((IFG2&UCA0TXIFG))						// TX buffer empty?
  				{
  					UCA0TXBUF =data[i];                    // TX  data
  					break;
  				}
  				time_delay(15);
  				if(j%5000==0)
  				{
  					HandleWdt();
  				}
  				if(j>=50000)
  				{
  					break;
  				}
  			}
  				//while ((!(IFG2&UCA0TXIFG))&& wdt<=18000);             // USCI_A0 TX buffer ready?
  				
  		}
  		_NOP();
}

// Send ACKACK to the motherunit that confirms that the sensors has received > "Start measurment"
unsigned char start_ACK()
{
	unsigned char status=0;
		SEND[0]=SENSORTYPE;
		SEND[1]='A';					// Put in ACKACK to the SEND array 
		SEND[2]='C';
		SEND[3]='K';
		SEND[4]='A';
		SEND[5]='C';
		SEND[6]='K';
		if(BACKUPPT100==1){		// gammal typkod f�r VBS men med PT100 backupm�jlighet
			SEND[7]=0xAA;}
		else{
			SEND[7]=0;
		}
		SEND[8]=CurrentFirmwareVersion;
		HandleWdt();
		convert_to_API_struct();		// Converts the SEND array to API structure
		transmitt_uart(send_data,27);	// Sends the API message to the motherunit
		status=receive_XBee(11,1,0x8B);		// Receives the status of the ACKACK message, returns the status byte in the response message
		if(status==0){
			return 0x00;}
		else{
			return 0xFF;}
		_NOP();
}
// program the Xbee unit with AT parameters
unsigned int AT_prog(unsigned long interval, unsigned int command)
{
	unsigned char AT_message[12]={0x7E,0x00,0x08,0x08,0x52,0x00,0x00,0x00,0x00,0x00,0x00,0x00}; // Structure of AT message in API mode
	unsigned long shiftmask[4]={0xFF000000,0xFF0000,0xFF00,0xFF};								// Mask to mask out each byte in a type long
	
	unsigned char temp_checksum=0;																// Checksum for transmitted message
	int OK;																						// Variable to determine if message was received correctly by the XBee node
	int i=0;
	P2OUT &= ~0x04;
	if(command==0x4448){
		interval=destH;}
		else if(command==0x444C){
			interval=destL;}
			else if(command=='WR'){
				AT_message[2]=0x04;
				AT_message[5]=(char)((command&0xFF00)>>8);
				AT_message[6]=(char)(command&0x00FF);
				AT_message[7]=0xFC;
				transmitt_uart(AT_message,8);					// Send AT command and data to XBee
				OK=receive_XBee(9,2,0x88);								// Wait for AT-receive ack
				return 0;
			}
			AT_message[5]=(char)((command&0xFF00)>>8);		
			AT_message[6]=(char)(command&0x00FF);
			
			for(i=0;i<4;i++)					// Shift in interval time to AT_message array
			{
				
				AT_message[i+7]=(char)((interval & shiftmask[i])>>(24-8*i));			// Shift in the interval data
			}
					
			
			for(i=3;i<=10;i++) // Calculate checksum
			{
				temp_checksum=temp_checksum+AT_message[i];								// Summing data to calculate checksum
			}
			
				temp_checksum=0xFF-(temp_checksum & 0x00FF);		// Checksum calculation
				AT_message[11]=temp_checksum;			
				HandleWdt();
				transmitt_uart(AT_message,12);					// Send AT command and data to XBee
				OK=receive_XBee(9,2,0x88);								// Wait for AT-receive ack
				
				if(OK==0)										// Check if data is ACK
				{
					return 0;
				}
				else if (OK==1)
				{
					return 0xFF;
				}	
	return 0xFF;
}


void one_sec_delay()						// One second delay
{
	unsigned int i;
	for(i=0;i<10;i++)
	{
		time_delay(0xCA00);
		HandleWdt();
		time_delay(0xCA00);
	}
}

// Used when the XBee node should go from transparent mode to API mode after remote update
unsigned char transparent_API_on()
{
	
	unsigned char trsp_ATcommand[3]={0x2B,0x2B,0x2B};
	unsigned char API_on_ATcommand[6]={0x41,0x54,0x41,0x50,0x31,0x0D};
	unsigned char exit_ATcommand[5]={0x41,0x54,0x43,0x4E,0x0D};
	unsigned char OK_command[3]={0x4F,0x4B,0x0D};
	unsigned char atcommandwr[5]={0x41,0x54,0x57,0x52,0x0D};
	unsigned int i=0;
	unsigned char ApiCheck[8]={0x7E,0x00,0x04,0x08,0x52,0x41,0x50,0x14};
	//one_sec_delay();
	one_sec_delay();
	do{
		while(((P2IN & 0x01)==1));
		transmitt_uart(trsp_ATcommand,3);				// Send "+++"
		receive_XBee(3,5,0xFF);
		if(strncmp(&receive[0],&OK_command[0],3)==0)
		{
			transmitt_uart(API_on_ATcommand,6);
			receive_XBee(3,5,0xFF);
			one_sec_delay();
			transmitt_uart(atcommandwr,5);
			receive_XBee(3,5,0xFF);
			one_sec_delay();
			transmitt_uart(exit_ATcommand,5);
			receive_XBee(3,5,0xFF);
			
			break;
		}
		i++;
	}while(i<4);
	
	if(i<4)
	{
		transmitt_uart(ApiCheck,8);
		receive_XBee(10,2,0x88);
		if(receive[8]==0x01)
		{
			API_mode=1;
			return 1;
		}
		else
		return 0;
	}
	else{return 0;}
}
// Imp meas "main" program

unsigned char impMeasSeq(void)
{

	unsigned char i=0;
	
	unsigned char control_AD5933_power_down[2] = {0x80, 0xA1};
	unsigned long imp[5];
	float temp=0;
	float Meanimp=0;
	unsigned long tempImp=0;
	unsigned long A2=0, A1=0, A0=0, B1=0, B0=0;
	
	HandleWdt();
	for(i=0;i<5;i++)
	{
		measuring_sequence();				// Measure the impedance with the AD5933
		imp[i]=imp_calculation();			// Calculate the impedance	
		time_delay(5000*i); 				// Time delay of different time to prevent the tires from affecting the measurement
	}
			  	
		transmitt_data(control_AD5933_power_down,2);
		//tempImp=(bubbleSort(imp,5)&0xFFFF00)>>8;;
		Meanimp=(float)tempImp;	
		HandleWdt();
		if(TemperatureMem/10>=0)
		{
			A2=((long)(A2_1)<<16)&0xFF0000;
			A2|=((long)(A2_2<<8))&0xFF00;
			A2|=((long)(A2_3))&0xFF;
			A1=((long)(A1_1)<<16)&0xFF0000;
			A1|=((long)(A1_2)<<8)&0xFF00;
			A1|=((long)(A1_3))&0xFF;
			A0=((long)(A0_1)<<16)&0xFF0000;
			A0|=((long)(A0_2)<<8)&0xFF00;
			A0|=((long)(A0_3))&0xFF;
			HandleWdt();
				
			Meanimp=Meanimp+(((float)A2/10000)*(TemperatureMem/10)*(TemperatureMem/10))+(((float)A1/1000)*(TemperatureMem/10))-((float)A0/10);
		}
		else if(TemperatureMem/10<0)
		{
			B1=((long)(B1_1)<<16)&0xFF0000;
			B1|=((long)(B1_2<<8))&0xFF00;
			B1|=((long)(B1_3))&0xFF;
			B0=((long)(B0_1)<<16)&0xFF0000;
			B0|=((long)(B0_2)<<8)&0xFF00;
			B0|=((long)(B0_3))&0xFF;
			HandleWdt();
			Meanimp=Meanimp-((((float)B1/1000)*(abs(TemperatureMem)/10))+((float)B0/100));
		}	
		tempImp=(long)(Meanimp);
			  //	P6OUT&=~0x20;										// Disable GND to imp. sensor
			  	/*SEND[0]=(char)((tempImp & 0xFF000000)>>24);		// Put the median impedance value into the SEND array
				SEND[1]=(char)((tempImp & 0x00FF0000) >> 16);
				SEND[2]=(char)((tempImp & 0x0000FF00) >> 8);
				SEND[3]=(char)(tempImp & 0x000000FF);	
				*/

				SEND[1]=(char)((tempImp & 0xFF00)>>8);
				SEND[2]=(char)((tempImp & 0xFF));
				SEND[3]=0x00;
				
				
			
			  	return 1;
}

void intToAscii(unsigned int hex)
{
	unsigned int hundreds;
	unsigned int tens;
	unsigned int rest;
	unsigned long ascii=0;
	
	if(hex<1000){
	hundreds=hex/100;
	tens=(hex%100)/10; 
	rest=(hex%10);
	ascii=((((unsigned long)hundreds+0x30)&0xFF)<<16);
	ascii|=(tens+0x30)<<8;
	ascii|=(rest+0x30);
	intToAsciiConverted=ascii;
	}
	else{
		intToAsciiConverted=0;}
}

void shdnMeasCard(){
	P3SEL&=~0x06;
	P3DIR|=0x06;
	P3OUT&=~0x06;
	P1OUT &=~0x02;									// Disable the +3V power for meascard
	//P3DIR|=0x06;
	//P3OUT&=~0x06;

}

void convert_to_SEND_buffer(){
	unsigned int temp=0;

	SEND[0]=SENSORTYPE;

	//TEST END
	if(SENSORTYPE==1){
		SEND[1]=0xFF;
		SEND[2]=(DeepTemperature&0xFF00)>>8;
		SEND[3]=DeepTemperature&0x00FF;
		SEND[4]=(SurfaceTemeprature&0xFF00)>>8;
		SEND[5]=SurfaceTemeprature&0x00FF;
		SEND[6]=(BatteryLevel&0xFF00)>>8;
		SEND[7]=BatteryLevel;
	}
	else{
		MeanConductivity=MeanConductivity/2;
		SEND[1]=(MeanConductivity&0xFF00)>>8;
		SEND[2]=MeanConductivity&0x00FF;
		SEND[3]=(SurfaceTemeprature&0xFF00)>>8;
		SEND[4]=SurfaceTemeprature&0x00FF;
		SEND[5]=(DeepTemperature&0xFF00)>>8;
		SEND[6]=DeepTemperature&0x00FF;
		//Battery value
		temp=BatteryLevel;
		temp=(((float)((unsigned long)temp)/1000)/0.05)+0.5;
		temp=floor(temp);
		SEND[7]=(char)temp; //battery
	}
	//Testmode f�r restsalt
	/*SEND[1]=(ConductivityPtoN&0xFF00)>>8;
	SEND[2]=ConductivityPtoN&0x00FF;
	SEND[3]=(ConductivityNtoP&0xFF00)>>8;
    SEND[4]=ConductivityNtoP&0x00FF;
    *////////////////
	SEND[8]=CurrentFirmwareVersion;

}

void ConductivityReset(){
	ConductivitySampleNumber=0;
	ConductivitySum=0;
	MeanConductivity=0;
	ConductivityPtoN=0;
	ConductivityNtoP=0;
}
