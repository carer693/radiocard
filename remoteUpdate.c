#include "msp430x24x.h"
#include "definitions.h" 
#include "remoteUppdate.h" 
#include "globals.h" 
unsigned int IntexHexAsciiToInt (unsigned char data1, unsigned char data2)
{
	unsigned int result = 0;
	if ((data1) >= 'A')
	{
		result = 16*result + (data1) - 'A' + 10;
	}
	else
	{
		result = 16*result + (data1) - '0';
	}	
	if ((data2) >= 'A')
	{
		result = 16*result + (data2) - 'A' + 10;
	}
	else
	{
		result = 16*result + (data2) - '0';
	}	
	return result;
}

void remoteUpdate(){
	
unsigned int *firmwarePlacement;			//Pointer to segment placement
unsigned char FWQU[33]={0x7E,0x00,0x1D,0x10,0x00,0x00,0x13,0xA2,0x00,0x00,0x00,0x00,0x00,0xFF,0xFE,0x00,0x00,'F','W',' ','Q','U','E','R','Y',':','R',':','0','0','0','0',0x00};
unsigned int tmpint=0;
unsigned char i=0;
unsigned char t=0;
unsigned char tempchecksum=0;


	
	if(AT_prog(ZERO,SLEEPMODE)!=1){					// normal sleep mode (always awake)
		  	initXbee();}
		  	if(AT_prog(ZERO,SLEEPMODE)!=1){
		  		_NOP();}
		  	
	AT_prog(ONE,DIGI6);
	AT_prog((unsigned long)(destH),DESTHIGH);	// Program the motehernode address to be able to send in transparent mode
	AT_prog((unsigned long)(destL),DESTLOW);
	FWQU[9]=(unsigned char)((destL&0xFF000000)>>24);
	FWQU[10]=(unsigned char)((destL&0xFF0000)>>16);
	FWQU[11]=(unsigned char)((destL&0xFF00)>>8);
	FWQU[12]=(unsigned char)(destL&0xFF);
	
	firmwarePlacement = (unsigned int *) 0xFE00;	//0xFE00 is the address where active sector variable is
	_DINT();
	//OBS! DETTA SKA EJ VARA MED I SENARE VERSIONER: ENDAST F�RSTA!!!!!
	FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
	FCTL3 = FWKEY;                     // Clear Lock bit
	*firmwarePlacement = 0x4141;
	FCTL1 = FWKEY;            // Clear WRT bit
  	FCTL3 = FWKEY + LOCK;     // Set LOCK bit
  	/////////////////////////////////////////////////////////////////////
  	if(*firmwarePlacement == 0x4141)	//Are we running in sector A?
	{
		memorySpacePlacement = 'B'; 	//Sector B is not used and will be used for the new firmware
		FWQU[31]='A';   //Running in sector A
		Flash_pnt = (unsigned int *) 0x8800;	//Address of sector B
	}
	else if(*firmwarePlacement == 0x4242)
	{
		memorySpacePlacement = 'A'; 	//Sector A is not used and will be used for the new firmware
		FWQU[31]='B';  //Running in sector B
		Flash_pnt = (unsigned int *) 0x1200;	//Address of sector A
	}
	tmpint=CurrentFirmwareVersion&0x00FF;
	intToAscii(tmpint);
	FWQU[28]=(char)((intToAsciiConverted&0xFF0000)>>16);
	FWQU[29]=(char)((intToAsciiConverted&0x00FF00)>>8);
	FWQU[30]=(char)((intToAsciiConverted&0x0000FF));
	HandleWdt();					//Toggle WDT
  	_EINT();
	for(i=3;i<=32;i++) // Calculate checksum
	{
		tempchecksum=tempchecksum+FWQU[i];								// Summing data to calculate checksum
	}	
	tempchecksum=0xFF-(tempchecksum & 0x00FF);		// Checksum calculation
	FWQU[32]=tempchecksum;
	transmitt_uart(FWQU,33);
	t=AT_prog(SEVEN,BAUDRATE);						
	BCSCTL1 = CALBC1_16MHZ; 					// Set the freq. of the DCO to 16MHz
	DCOCTL = CALDCO_16MHZ; 
//  UCA0BR0 = 104;                              // 16MHz 9600
//	UCA0BR1 = 0;                              // 16MHz 9600
//	UCA0MCTL = UCBRF_3 + UCOS16;								// UCBRFx = 11 and oversampling mode enable	
	UCA0BR0 = 8;                              // 16MHz 115200
	UCA0BR1 = 0;                              // 16MHz 115200
	UCA0MCTL = UCBRF_11 + UCOS16;								// UCBRFx = 11 and oversampling mode enable	
	t+=AT_prog(ZERO,API);
	API_mode=0;		
	
	
	if(BatteryLevel>2500)						// Battery level must be over 2.5V to permit update
	{
  		for(i=0;i<20;i++)
  		{
  			recDsubBuf[i]=0;
  		}
  		for(i=0;i<5;i++){
	  		if(receive_UART(0xFF,60000))					// Receive FW information message
	  		{
		  		if(!strncmp(&recDsubBuf[startDsubPnt],"FW DOWNLOAD RS:", 15)) 
				{
		
					newFirmwareVersion = (recDsubBuf[15]-0x30)*100;
					newFirmwareVersion += (recDsubBuf[16]-0x30)*10;
					newFirmwareVersion += recDsubBuf[17]-0x30;
						if(newFirmwareVersion > CurrentFirmwareVersion) 		//New firmwareversion?
						{
							uppdateFirmware();									// Start updating roadsensor	
							break;				
						}
						else{
							break;}			
					}
			}
  		}
	
			initClk();
			initUart();
			initXbee();
			initTimer();
			flushRecDsubBuf();
			
		
  		_NOP();
	}
}

void flushRecDsubBuf (void)
{
	startDsubPnt=0;
	endDsubPnt=0;
}

unsigned char uppdateFirmware ()
{
	unsigned int currentSector=0;
	unsigned char firmwareUppdateBuffer[100];
	unsigned int i=0;
	unsigned int k=0;
	unsigned int iTemp=0;
	unsigned int temp=0;
	unsigned long int eepromPointer=0x20000;
	unsigned long int j=0;
	unsigned char tmp=0;
	unsigned char fwData=0;
	unsigned char fwData2=0;
	unsigned long int TempeepromPointer=0;
	unsigned long int fwSize=0;
	unsigned char checkSumFW=0;
	unsigned int byteCountFW;
	unsigned long int flashPointer2=0;
	unsigned char endOFFile=0;
	unsigned char fwDownloadError=0;
	unsigned long int dataRecieved=0;
	unsigned char tt=0;
	unsigned int checksumFwFile=0;
	unsigned long temporaryTimer=0;
	unsigned int tempData=0;
	unsigned int tempFlash=0;
	unsigned int interuptVectorBuff[64];
	unsigned int interuptVectorAdrBuff[64];
	unsigned int oldInteruptBuff[64];
	unsigned char index=0;
	unsigned int AA=0x4141;
	unsigned int BB=0x4242;
	
	//st�ng av alla interrupt!!!
 	//clock_init16Mhz_BTuart115200(); //Enter fast mode, OBS gl�m ej i denna funktion �ndra uartinst�llningar f�r 16Mhz
 	UC1IE &= ~UCA1RXIE; //St�ng av uart interrupt
    //UC0IE &= ~UCA0RXIE; //St�ng av uart interrupt
	//h�mta storleken p� filen som xbee har skickat
	i=0;
	do{
		i++;
	}while(recDsubBuf[i] != 0x0A);
	i=i-2;
	j=1;
	do{
		fwSize += j*(recDsubBuf[i] - 0x30);
		i--;
		j=j*10;
	}while(recDsubBuf[i] != ':');
	_EINT();
	TACCTL0 &=~CCIE;										// Interrupt Disabled - System time
	TBCCTL0 &=~CCIE;
	IE2 |= UCA0RXIE;
	i=0;

		//Skicka ett komando till uppdateraren att du vill ha filen

			firmwareUppdateBuffer[i++]='F';
			firmwareUppdateBuffer[i++]='W';
			firmwareUppdateBuffer[i++]=' ';
			firmwareUppdateBuffer[i++]='G';
			firmwareUppdateBuffer[i++]='E';
			firmwareUppdateBuffer[i++]='T';
			firmwareUppdateBuffer[i++]=0x0D;
			firmwareUppdateBuffer[i++]=0x0A;
			flashEraseMSP();
			HandleWdt();
			//Skicka kommandot FW get			
			if(!(P2IN&0x08)&& i!=0) //CTS low and data to send
			{
				temp=i;
				i=0;
				do
				{
					HandleWdt();
					//RTS CTS??
					if ((IFG2&UCA0TXIFG)&& (!(P2IN&0x08)))		//Buffer ready to send character? CTS low?
					{																			 																				// Aktivera s�ndning p� UART0
						UCA0TXBUF = firmwareUppdateBuffer[i];
						i++;
					}	
				}while(i<temp); //Send data
			}
			i=0;
			j=0;
			_NOP();
			HandleWdt();
			//WDToff();
			//while(!(IFG2&UCA0RXIFG)); //v�nta p� f�rsta byten i filen
			endDsubPnt=0;
			startDsubPnt=0;
			//Test av ny uppdatering
			do
			{
				if(endDsubPnt>0){
				if(recDsubBuf[endDsubPnt-1] == 0x0A)
				{
					j=0;
					P2OUT |= 0x04; //RTS high. No more data to MSP
					//P4OUT &= ~BIT5; //Green led on
					temporaryTimer=0;
					do
					{
						firmwareUppdateBuffer[i++] = recDsubBuf[j++];
						fwSize--;
						dataRecieved++;
					}while (firmwareUppdateBuffer[i-1] != 0x0A);
					HandleWdt();
					if(endOFFile==0){
						i=0;
						do
						{
							checksumFwFile^=firmwareUppdateBuffer[i++];
						}while (firmwareUppdateBuffer[i-1] != 0x0A);
						checkSumFW=0;
						byteCountFW=0;
						i=0;					
						if(firmwareUppdateBuffer[i]!= ':')
						{
							_NOP(); //Error
							fwDownloadError=1;
							break;
						}
						i++;
						fwData=firmwareUppdateBuffer[i++]; //Byte count
						fwData2=firmwareUppdateBuffer[i++];
						byteCountFW=IntexHexAsciiToInt(fwData, fwData2);	
						checkSumFW=byteCountFW;			
						fwData=firmwareUppdateBuffer[i++]; //Address
						fwData2=firmwareUppdateBuffer[i++];
						checkSumFW+=IntexHexAsciiToInt(fwData, fwData2);
						flashPointer2=IntexHexAsciiToInt(fwData, fwData2);
						flashPointer2=flashPointer2<<8;
						fwData=firmwareUppdateBuffer[i++];				
						fwData2=firmwareUppdateBuffer[i++];
						checkSumFW+=IntexHexAsciiToInt(fwData, fwData2);
						flashPointer2+=IntexHexAsciiToInt(fwData, fwData2);
						Flash_pnt = (unsigned int *) flashPointer2;
						
						fwData=firmwareUppdateBuffer[i++]; //Record type
						fwData2=firmwareUppdateBuffer[i++];
						checkSumFW+=IntexHexAsciiToInt(fwData, fwData2);
						if(fwData2=='1')
						{
							//end of file
							endOFFile=1;
						}		
						//_DINT();
						FCTL2 = FWKEY + FSSEL_1 + FN4;          	// Flash Timing Generator ~460kHz
						FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
						FCTL3 = FWKEY;                  // Clear Lock bit		
						
						for(j=0; j<byteCountFW/2; j++)
						{
							fwData=firmwareUppdateBuffer[i++];
							fwData2=firmwareUppdateBuffer[i++];
							tempData=IntexHexAsciiToInt(fwData, fwData2);
							checkSumFW+=IntexHexAsciiToInt(fwData, fwData2);
							
							fwData=firmwareUppdateBuffer[i++];
							fwData2=firmwareUppdateBuffer[i++];
							tempData|=(IntexHexAsciiToInt(fwData, fwData2))<<8;
							checkSumFW+=IntexHexAsciiToInt(fwData, fwData2);
							
							if(Flash_pnt<(unsigned int*)0xFFC0){
								*Flash_pnt=tempData;
							   
								tempFlash=*Flash_pnt;
								if(*Flash_pnt != tempData) //Something went wrong
								{
									_NOP();
									fwDownloadError=1;
									break;
								}
							*Flash_pnt++;
							}
							else{
							    interuptVectorAdrBuff[index]=(unsigned int)Flash_pnt++;
								interuptVectorBuff[index]=tempData;
								index++;}
						}
						FCTL1 = FWKEY;                            // Clear WRT bit
						FCTL3 = FWKEY + LOCK;                     // Set LOCK bit
						//_EINT();
						fwData=firmwareUppdateBuffer[i++];
						fwData2=firmwareUppdateBuffer[i++];
						checkSumFW^=0xFF;
						checkSumFW=checkSumFW+1;
						if(checkSumFW != IntexHexAsciiToInt(fwData, fwData2))
						{
							//Error
							_NOP();
							fwDownloadError=1;
							break;
						}
						i=0;
						j=0;
						while(recDsubBuf[j++] != 0x0A); //Find 0x0A in buffer
						for(i=0; j<endDsubPnt; i++)
						{
							firmwareUppdateBuffer[i] = recDsubBuf[j++];
							fwSize--;
							dataRecieved++;
						}
					}
					flushRecDsubBuf();
					//flushBTbuff();
					P2OUT &=~ 0x04; //RTS low
					//P4OUT |= BIT5; //Green led on
					P5OUT^=0x06;
				}
			}
			}while(fwSize!=0 && fwDownloadError==0 && temporaryTimer++<900000);
			IE2 &=~UCA0RXIE;
			if(checksumFwFile != firmwareUppdateBuffer[0])
			{
				fwDownloadError=1;
			}
			if(temporaryTimer>=900000)
			{
				fwDownloadError=1;
			}			
			_EINT();
			i=0;
			firmwareUppdateBuffer[i++]='F';
			firmwareUppdateBuffer[i++]='W';
			firmwareUppdateBuffer[i++]=' ';
			if(fwDownloadError==0 && endOFFile==1) //FW download ok
			{
				firmwareUppdateBuffer[i++]='D';
				firmwareUppdateBuffer[i++]='O';
				firmwareUppdateBuffer[i++]='W';
				firmwareUppdateBuffer[i++]='N';
				firmwareUppdateBuffer[i++]='L';
				firmwareUppdateBuffer[i++]='O';
				firmwareUppdateBuffer[i++]='A';
				firmwareUppdateBuffer[i++]='D';
				firmwareUppdateBuffer[i++]='E';
				firmwareUppdateBuffer[i++]='D';
			}
			else
			{
				firmwareUppdateBuffer[i++]='F';
				firmwareUppdateBuffer[i++]='A';
				firmwareUppdateBuffer[i++]='I';
				firmwareUppdateBuffer[i++]='L';
				firmwareUppdateBuffer[i++]='E';
				firmwareUppdateBuffer[i++]='D';
			}
			firmwareUppdateBuffer[i++]=0x0D;
			firmwareUppdateBuffer[i++]=0x0A;			
			if(!(P2IN&0x08)&& i!=0) //CTS low and data to send
			{
				temp=i;
				i=0;
				do
				{
					HandleWdt();
					//RTS CTS??
					if ((IFG2&UCA0TXIFG)&& (!(P2IN&0x08)))		//Buffer ready to send character? CTS low?
					{																			 																				// Aktivera s�ndning p� UART0
						UCA0TXBUF = firmwareUppdateBuffer[i];
						i++;
					}	
				}while(i<temp); //Send data
			}
			_NOP();
			if(fwDownloadError==1){
				return 0xFF;}
			if(fwDownloadError==0 && endOFFile==1)
			{
				Flash_pnt=(unsigned int*)0xFFC0;
				for(i=0;i<32;i++){  // 64 addresses between FFC0 and FFFF. Each Flash_pnt++ increases the address by 2
				oldInteruptBuff[i]=*Flash_pnt++;  // Save the old interrupt vectors
				}
				Flash_pnt=(unsigned int*) 0xFE00;
				currentSector=*Flash_pnt;    // Save current sector
				FlashEraseInteruptSectors();  // Erase interrupt vectors including firmware placement
				FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
				FCTL3 = FWKEY;                  // Clear Lock bit	
				for(i=0;i<index;i++){
					Flash_pnt=(unsigned int*) interuptVectorAdrBuff[i];  // Write the new interrupt vectors to flash
					iTemp=(unsigned int)interuptVectorBuff[i];
					*Flash_pnt=iTemp;
					tempData=*Flash_pnt;
					if(*Flash_pnt != interuptVectorBuff[i]) //Something went wrong
					{
						_NOP();
						fwDownloadError=1;
						break;
					}
				}
				if(fwDownloadError==1){ // Something went wrong while writing the new interrupt vector to flash
					FlashEraseInteruptSectors();
					FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
					FCTL3 = FWKEY;                  // Clear Lock bit	
					Flash_pnt=(unsigned int*) 0xFFC0;
					for(i=0;i<32;i++){
						*Flash_pnt++=oldInteruptBuff[i];}  // Re write the old interrupt vectors to flash
						Flash_pnt = (unsigned int *) 0xFE00;
						if(currentSector==BB){	// Write back the firmware placement
							*Flash_pnt = BB;}
						else if(currentSector==AA){
							*Flash_pnt = AA;}		//set to AA
					
				}
				else{
					Flash_pnt = (unsigned int *) 0xFE00; // New interrupt vectors is successfully written to flash
					if(currentSector==AA){	//New firmware placement 
						*Flash_pnt = BB;}
					else if(currentSector==BB){
						*Flash_pnt = AA;	}		//set to AA
					}
					currentSector=*Flash_pnt;
					FCTL1 = FWKEY;                            // Clear WRT bit
					FCTL3 = FWKEY + LOCK;                  //set to BB
				
			}
			//fwData=readExtEeprom2(0x000000);
			_NOP();
			for(j=0; j<300000;j++)
  			{
  				_NOP();
  			}
 			for(j=0; j<300000;j++)
  			{
  				_NOP();
  			}
			for(j=0; j<300000;j++)
  			{
  				_NOP();
  			}
 			for(j=0; j<300000;j++)
  			{
  				_NOP();
  			}
 			for(j=0; j<300000;j++)
  			{
  				_NOP();
  			}
  		/*	_DINT();																			// Disable interrupts
			IE1 &= ~(NMIIE);
			Flash_clr( (int *) INFOC_MEM );				// erase segment C 
			_EINT();
			IE1 |= NMIIE;	*/
  			FCTL1 = 0; //Restart
}
void FlashEraseInteruptSectors(){
	unsigned long sector;
	unsigned int *activeSector;
	for(sector=0xFFC0; sector<=0xFFFF; sector = sector+512) //Earse sector B
		{
			FCTL1 = FWKEY + ERASE;                    // Set Erase bit
  			FCTL3 = FWKEY;                            // Clear Lock bit
  			activeSector = (unsigned int *) sector;
  			*activeSector = 0;
  			HandleWdt();
		}
	FCTL1 = FWKEY;                            // Clear WRT bit
	FCTL3 = FWKEY + LOCK;                     // Set LOCK bit
}
void flashEraseMSP (void)
{
	unsigned int sector;
	unsigned int *activeSector;
	FCTL2 = FWKEY + FSSEL_1 + FN4;          	// Flash Timing Generator ~460kHz
	HandleWdt();					//Toggle WDT
	_DINT();							//Disable interrupt
	activeSector = (unsigned int *) 0xFE00;	//Get the address of active sector
	if(*activeSector == 0x4141)
	{
		for(sector=0x8800; sector<0xFE00; sector = sector+512) //Earse sector B
		{
			FCTL1 = FWKEY + ERASE;                    // Set Erase bit
  			FCTL3 = FWKEY;                            // Clear Lock bit
  			activeSector = (unsigned int *) sector;
  			*activeSector = 0;
  			HandleWdt();
		}
	}
	else
	{
		for(sector=0x1200; sector<0x8800; sector = sector+512) //Erase sector A
		{
			FCTL1 = FWKEY + ERASE;                    // Set Erase bit
  			FCTL3 = FWKEY;                            // Clear Lock bit
  			activeSector = (unsigned int *) sector;
  			*activeSector = 0;
  			HandleWdt();
		}		
	}
	FCTL1 = FWKEY;                            // Clear WRT bit
	FCTL3 = FWKEY + LOCK;                     // Set LOCK bit
	_EINT();	//Enable interrupt
}

	
