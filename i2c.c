#include "msp430x24x.h"
#include "i2c.h"
#include <string.h>
#include "definitions.h"
void init_i2c (void)
{
	//SLAVE MSP430FR5730
/*    // Configure Pins for I2C
    P1SEL1 |= BIT6 + BIT7;                  // Pin init
    // eUSCI configuration
    UCB0CTLW0 |= UCSWRST ;	            //Software reset enabled
    UCB0CTLW0 |= UCMODE_3  + UCSYNC;	    //I2C mode, sync mode
    UCB0I2COA0 = 0x48 + UCOAEN;   	    //own address is 0x48 + enable	
    UCB0CTLW0 &=~UCSWRST;	            //clear reset register
    UCB0IE |=  UCRXIE0; 	            //receive interrupt enable	*/
    //P3DIR&=~0x06;
    //P3REN&=~0x06;
    //P3OUT&=~0x06;
	P3SEL |= 0x06;                            // Assign I2C pins to USCI_B0
	UCB0CTL1 |= UCSWRST;                      // Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;     // I2C Master, synchronous mode
    UCB0CTL1 = UCSSEL_2 + UCSWRST;            // Use SMCLK, keep SW reset
	UCB0BR0 = 12;                             // fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA = 0x48;                         // Slave Address is 048h
	UCB0CTL1 &= ~UCSWRST;                     // Clear SW reset, resume operation
	//IE2 |= UCB0RXIE;                          // Enable RX interrupt
	i2cStatus=S_NOP;
	i2cEndPnt=i2cStartPnt=0; 
	
	  
	
}

unsigned int IntToAscii (unsigned char *s)
{
	unsigned int i,j;
	unsigned int crc=0;
	j = strlen((const char*)s);
	crc=*s; //First byte
	s++;	
	for(i=1;i<j;i++)
	{
		crc+=*s;
		s++;
	}
	return (((digits[(crc&0x00F0)>>4])<<8)+(digits[crc&0x000F]));
}

unsigned int AsciiToInt (unsigned char *s)
{
	unsigned int result = 0;
	int i;
	for (i=0; i<2; i++, s++)
	{
		if ((*s) >= 'A')
		{
			result = 16*result + (*s) - 'A' + 10;
		}
		else
		{
			result = 16*result + (*s) - '0';
		}
	}
	return result;
}

unsigned char checkI2CChecksum(void)
{
	unsigned char crc, i;
	i=0;
	crc = i2cBuffer[i];	
	for (i=1; i2cBuffer[i] != '*'; i++) //Do between > and *
	{
		crc+=i2cBuffer[i];					//XOR data	
	}
	crc+=i2cBuffer[i]; //Add *
	if(crc == AsciiToInt(&i2cBuffer[i+1]))	//Convert checksum to integer and check if same
	{
		return 1;
	}
	return 0;		
}



void i2cHandler (void)
{
  char* tpnt;
	//Switch case that handles the communication between two MSP430
switch (i2cStatus)
	{				
		case S_NOP:	//Status No Operation
		{
			break;
		}
		case D_START:	//SEND Data START to slave
		{
			createI2CMessage('G',0,0,0,0x10);	
			i2cStatus=D_SENDING;
			break;
		}
		case D_SENDING:
		{
			if(!(IE2&UCB0TXIE)) //TX interrupt enabled?
			{
				IE2 |= UCB0TXIE;   // Enable TX interrupt
				UCB0I2CIE |= UCNACKIE;	//Enabla nack interrupt
				while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
    			UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition		
			}
			if(i2cStartPntOut>=i2cEndPntOut) //All data sent
			{
				//while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
				//IE2 &= ~UCB0TXIE;   // Disable TX interrupt
				i2cStatus=D_RECEIVE; //Receive data from slave
			}
			break;
		}
		case D_RECEIVE:
		{
			if(i2cEndPnt>2){
				if((tpnt=strchr(i2cBuffer,'<'))!=0x00){
					if((tpnt-i2cBuffer)<i2cEndPnt){
																				//if((i2cBuffer[i2cEndPnt-1]=='<') || (i2cBuffer[i2cEndPnt-2]=='<')){   //Check for '<'
			
				//IE2 &= ~UCB0RXIE;						//Disable RX interrupt
				if(checkI2CChecksum())
				{
					//DATA OK
					i2cStatus=D_RECEIVED;	
				}
				else
				{
					//DATA Checksum fail
					i2cStatus=D_CRC_FAIL;
				}
			}
			}
			}
			break;
		}
		case D_RECEIVED:
		{
			switch (i2cBuffer[1])
			{
				case 'G': //general message
				{
					if(strstr((const char*)&i2cBuffer[0], "OK"))
					{
						if(i2cInitOk==0) //StartI2C responce?
						{
							i2cInitOk=1; //I2C init done!
							i2cEndPnt=i2cStartPnt=0; //Reset buffer
							i2cStatus=S_NOP;
						}
						else if(i2cStopOk==0) //StopI2C responce?
						{
							i2cStopOk=1;
							i2cEndPnt=i2cStartPnt=0; //Reset buffer
							i2cStatus=S_NOP;
							P3SEL &= ~0x06; 
							UCB0CTL1 |= UCSWRST; //Disable I2C module  
							IE2 &= ~UCB0TXIE;   // Disable TX interrupt 
							IE2 &= ~UCB0RXIE;   //Disable RX interupt
							UCB0I2CIE &= ~UCNACKIE;	//Disable nack interrupt        
						}
					}
					break;
				}
				case 'D': //Data message
				{
					i2cDataReceived=1; //Tell software that data is received in buffer
					i2cStatus=S_NOP;
					break;
				}
				case 'E': //Error
				{
					//TODO??
					break;
				}
				default:
				{
					break;
				}
			}
			break;
		}
		case D_CRC_FAIL:
		{
			//TODO??
			break;
		}
		default:
		{
			break;
		}
	}
}		

void createI2CMessage (unsigned char type, unsigned char sensor, unsigned char wires, unsigned char channel, unsigned char status_c)
{
	unsigned int crc=0;
	if((status_c&BIT4))
	{
		i2cStartPntOut=i2cEndPntOut=0; //Clear buffer
	}
	switch (type)
	{
		case 'G': //General message
		{
			if(i2cStatus==D_START) // Start message to slave
			{
				i2cBufferOut[i2cEndPntOut++] = '>';
				i2cBufferOut[i2cEndPntOut++] = 'G';
				i2cBufferOut[i2cEndPntOut++] = ' ';
				i2cBufferOut[i2cEndPntOut++] = 'S';
				i2cBufferOut[i2cEndPntOut++] = 'T';
				i2cBufferOut[i2cEndPntOut++] = 'A';
				i2cBufferOut[i2cEndPntOut++] = 'R';
				i2cBufferOut[i2cEndPntOut++] = 'T';
				i2cBufferOut[i2cEndPntOut++] = '*';
				i2cBufferOut[i2cEndPntOut] = 0x00; //Null terminator in IntToAscii function
				crc=IntToAscii(&i2cBufferOut[0]);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0xFF00)>>8);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0x00FF));
				i2cBufferOut[i2cEndPntOut++] = '<';
			}
			else //Stop message to slave
			{
				i2cBufferOut[i2cEndPntOut++] = '>';
				i2cBufferOut[i2cEndPntOut++] = 'G';
				i2cBufferOut[i2cEndPntOut++] = ' ';
				i2cBufferOut[i2cEndPntOut++] = 'S';
				i2cBufferOut[i2cEndPntOut++] = 'T';
				i2cBufferOut[i2cEndPntOut++] = 'O';
				i2cBufferOut[i2cEndPntOut++] = 'P';
				i2cBufferOut[i2cEndPntOut++] = 'P';
				i2cBufferOut[i2cEndPntOut++] = '*';
				i2cBufferOut[i2cEndPntOut] = 0x00; //Null terminator in IntToAscii function
				crc=IntToAscii(&i2cBufferOut[0]);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0xFF00)>>8);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0x00FF));
				i2cBufferOut[i2cEndPntOut++] = '<';
			}
			break;
		}
		case 'E': //Error message
		{
			i2cBufferOut[i2cEndPntOut++] = '>';
			i2cBufferOut[i2cEndPntOut++] = 'E';
			i2cBufferOut[i2cEndPntOut++] = ' ';
			i2cBufferOut[i2cEndPntOut++] = '*';
			i2cBufferOut[i2cEndPntOut] = 0x00; //Null terminator in IntToAscii function
			crc=IntToAscii(&i2cBufferOut[0]);
			i2cBufferOut[i2cEndPntOut++] = ((crc&0xFF00)>>8);
			i2cBufferOut[i2cEndPntOut++] = ((crc&0x00FF));
			i2cBufferOut[i2cEndPntOut++] = '<';			
		}
		case 'Q': //query meassage
		{
			if((status_c&0x10)) //First data
			{
				i2cBufferOut[i2cEndPntOut++] = '>';
				i2cBufferOut[i2cEndPntOut++] = 'Q';
				i2cBufferOut[i2cEndPntOut++] = ' ';
			}
			i2cBufferOut[i2cEndPntOut++] = sensor;
			i2cBufferOut[i2cEndPntOut++] = wires;
			i2cBufferOut[i2cEndPntOut++] = channel;
			if((status_c&0x01))
			{
				i2cBufferOut[i2cEndPntOut++] = ';';
			}
			else
			{
				i2cBufferOut[i2cEndPntOut++] = '*';
				i2cBufferOut[i2cEndPntOut] = 0x00; //Null terminator in IntToAscii function
				crc=IntToAscii(&i2cBufferOut[0]);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0xFF00)>>8);
				i2cBufferOut[i2cEndPntOut++] = ((crc&0x00FF));
				i2cBufferOut[i2cEndPntOut++] = '<';
			}
			break;
		}
	}
}


//Globala funktioner som ska anv�ndas i �vrig kod

void startI2C (void)
{
	i2cInitOk=0; // Reset statusvariabel for init
	init_i2c();  // Init regiser
	i2cStatus=D_START; //Send start
}

void sendI2CData (unsigned char type, unsigned char sensor, unsigned char wires, unsigned char channel, unsigned char status_sendi2c)
{
	i2cDataReceived=0; //Reset received data indication
	createI2CMessage(type, sensor, wires, channel, status_sendi2c);
	if((status_sendi2c & 0x01)==0) //this was all data, send message
	{
		i2cStatus=D_SENDING;
		i2cEndPnt=i2cStartPnt=0; //clear receive buffer
		IE2 &= ~UCB0TXIE;
	}
}

void stopI2C (void)
{
	i2cStopOk=0; //Reset statusvariabel
	sendI2CData('G', 0,0,0,0x10); //Genaral message stop
	i2cStatus=D_SENDING;//Send stop
	i2cEndPnt=i2cStartPnt=0; //clear receive buffer
}
unsigned char I2C_data_extract(void){

	unsigned char i=0;
	unsigned int j=0;
	unsigned char meastype=0;
	unsigned char endofdata=0;
	unsigned int sensordata=0;
	unsigned char channel=0;
	unsigned char wire=0;

	do{
		do{
			i++;
		}while(i2cBuffer[i]!='T' && i2cBuffer[i]!='S' && i<100);	// Find S or T in message
		if(i>=100){
			return 0;
			break;
		}
		meastype=i2cBuffer[i];
		channel=i2cBuffer[i+2];
		wire=i2cBuffer[i+1];
			// Find ; or : (next data or end of data)
		do{
				i++;
		}while(i2cBuffer[i]!=';' && i2cBuffer[i]!='*' && i<100);		// Find ; or '*' in message
		if(i>=100){
			return 0;
			break;
		}
		// Check if data is end of data or more data is to be extracted
		if(i2cBuffer[i]==';'){			// More data in buffer
			endofdata=0;
		}
		else if(i2cBuffer[i]=='*'){		// No more data in buffer
			endofdata=1;
		}
		if(meastype=='S'){
			i--; //First value
			sensordata=(i2cBuffer[i--]-0x30);
			j=1;
			if(i2cBuffer[i]!=':'){
			do
			{
				j=j*10;
				sensordata+=((i2cBuffer[i--]-0x30)*j);
			}while(i2cBuffer[i]!=':');//Last data point
			}
			if(wire=='2'){
				ConductivityPtoN=sensordata;
			}
			if(wire=='4'){
				ConductivityNtoP=sensordata;
			}
		}
		else if(meastype=='T'){
			i--; //First value
			sensordata=(i2cBuffer[i--]-0x30);
			i--; //Skip '.'
			j=1;
			do
			{
				j=j*10;
				sensordata+=((i2cBuffer[i--]-0x30)*j);
			}while((i2cBuffer[i]!=':') && (i2cBuffer[i]!='-'));//Last data
			if(i2cBuffer[i]=='-') //negative temp
			{
				sensordata=0xFFFF-((int)sensordata);
				sensordata++;
			}
			if(channel=='2'){
				SurfaceTemeprature=sensordata;
				if(SENSORTYPE==0x11||SENSORTYPE==0x14||BACKUPPT100){
					DeepTemperature=32767;
				}
			}
			else if(channel=='3'){
				DeepTemperature=sensordata;
				if(SENSORTYPE==0x11||SENSORTYPE==0x14||BACKUPPT100){
					SurfaceTemeprature=32767;
				}
			}
	}
	}while(endofdata!=1);
	return 1;


}

/*void convert_SEND_buffer(void)
{
	unsigned int temp;
	unsigned int sensordata=0;
	unsigned char i=0;
	unsigned int j=0;
	unsigned char meastype=0;
	//Battery value
	//temp=SEND[6];
	//temp=temp<<8;
  	//temp+=SEND[7];
	//temp=(temp/0.05);
  	//SEND[7]=(char)temp; //battery
  	
  	//Salt sensor value
  	//Find ';'
	for(i=0; i2cBuffer[i]!='S'; i++) //Find salt
  	{
  		_NOP();
  	}
  	//Find ';'
  	for(i=i; i2cBuffer[i]!=';'; i++)
  	{
  		_NOP();
  	}
  	i--; //First value
  	sensordata=(i2cBuffer[i--]-0x30);
  	j=1;
  	do
  	{
  		j=j*10;
  		sensordata+=((i2cBuffer[i--]-0x30)*j);
  	}while(i2cBuffer[i]!=':');//Last data
  	//SEND[1]=(sensordata&0xFF00)>>8;
  	//SEND[2]=sensordata&0x00FF;
  	
  	//Yttemp
  	for(i=0; i2cBuffer[i]!='T'; i++) //Find first Temp
  	{
  		_NOP();
  	}
  	//Find ';'
  	for(i=i; i2cBuffer[i]!=';'; i++)
  	{
  		_NOP();
  	}
  	i--; //First value  
  	sensordata=(i2cBuffer[i--]-0x30);
  	i--; //Skip '.'
  	j=1;
  	do
  	{
  		j=j*10;
  		sensordata+=((i2cBuffer[i--]-0x30)*j);
  	}while((i2cBuffer[i]!=':') && (i2cBuffer[i]!='-'));//Last data	
  	if(i2cBuffer[i]=='-') //negative temp
  	{
  		sensordata=0xFFFF-((int)sensordata);
  		sensordata++;
  	}
   //	SEND[3]=(sensordata&0xFF00)>>8;
  //    SEND[4]=sensordata&0x00FF;
  	
   	//Djuptemp
  	for(i=0; i2cBuffer[i]!='T'; i++) //Find first Temp
  	{
  		_NOP();
  	}
  	i++;
  	for(i=i; i2cBuffer[i]!='T'; i++) //Find second Temp
  	{
  		_NOP();
  	}
  	//Find '*'
  	for(i=i; i2cBuffer[i]!='*'; i++)
  	{
  		_NOP();
  	}
  	i--; //First value  
  	sensordata=(i2cBuffer[i--]-0x30);
  	i--; //Skip '.'
  	j=1;
  	do
  	{
  		j=j*10;
  		sensordata+=((i2cBuffer[i--]-0x30)*j);
  	}while((i2cBuffer[i]!=':') && (i2cBuffer[i]!='-'));//Last data	
  	if(i2cBuffer[i]=='-') //negative temp
  	{
  		sensordata=0xFFFF-((int)sensordata);
  		sensordata++;
  	}
   //	SEND[5]=(sensordata&0xFF00)>>8;
  //	SEND[6]=sensordata&0x00FF;
  //	SEND[8]=CurrentFirmwareVersion;
  	return sensordata
}
*/



#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void)
{ 
	if((UCB0CTL1&UCTR)&&(IE2&UCB0TXIE)) //transmitt mode, and TX IE enable
	//if((IE2&UCB0TXIE)) //transmitt mode, and TX IE enable
	{
		
		if (i2cStartPntOut<i2cEndPntOut)                // Check TX byte counter
	  	{
	  		UCB0TXBUF = i2cBufferOut[i2cStartPntOut++];     // Load TX buffer
	  	}
	  	else //All data sent
	  	{

	   		//UCB0CTL1 |= UCTXSTP;                    // I2C stop condition
	   		UCB0CTL1 &= ~UCTR; 						//Receive
	   		UCB0CTL1 |= UCTXSTT;					//generate repeted start_read
	    	IFG2 &= ~UCB0TXIFG;                     // Clear USCI_B0 TX int flag
	    	IE2 |= UCB0RXIE;						//Enable RX interrupt
	    	//UCB0TXBUF = i2cBufferOut[i2cStartPntOut++];     // Load TX buffer
	    	//IE2 &= ~UCB0TXIE;   					// Disable TX interrupt??
	  	}
	}
	else if((!(UCB0CTL1&UCTR))&&(IE2&UCB0RXIE)) //Receive mode, RX IE enable
	{
		i2cBuffer[i2cEndPnt++]=UCB0RXBUF;   // Get RX data
        if(i2cEndPnt>100)
        {
        	i2cEndPnt=0;
        }

			if(i2cBuffer[i2cEndPnt-1]=='<') //All data received, generate stop and receive one dummy byte more
			{
				UCB0CTL1 |= UCTXSTP; //Stop
			}

	}
}

/*
#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void)
{
	if(UCB0STAT&UCNACKIFG) //Nack interrupt
	{
		UCB0STAT &= ~(UCNACKIFG);       // Clear interrupt flags
		UCB0CTL1 |= UCTXSTP; // generate Stop
	}
}  */
