#ifndef MAIN_H_
#define MAIN_H_


extern unsigned int systemtime;
extern unsigned int wdt;
void time_delay(unsigned int delaytime_us);
void transmitt_data(unsigned char data[], unsigned char number_of_bytes);
void receive_data(unsigned char register_address,unsigned char number_of_bytes);
void startup_for_pt100(void);
void ADC12_measurment(unsigned char AD_mode);
void initiera_AD5933();
unsigned char impMeasSeq(void);
void preforme_ADC();
unsigned char transparent_API_on();
void convert_to_API_struct();
unsigned char measuring_sequence();
unsigned long imp_calculation();
unsigned int status_register_poll(unsigned char status_register, unsigned char poll_mask);
extern short int remoteUppdateFromDsub;
unsigned char receive_XBee(unsigned char bytes, unsigned char type, unsigned char frameType);
unsigned char start_ACK();
unsigned int AT_prog(unsigned long interval, unsigned int command);
void HandleWdt();
unsigned char MeasCardCom(unsigned char type, unsigned char n_wires, unsigned char port);

extern unsigned int newFirmwareVersion;
extern unsigned char i2cInitOk;
extern unsigned char i2cDataReceived;
extern unsigned char i2cStopOk;
extern unsigned long OperatingSleeptime;
//test
unsigned int nosaltmeas=0;
unsigned int notempmeas=0;
//
unsigned char TempAbove6=0;
unsigned int SaltMeasInterval=240;
unsigned char ActiveTempMeas=0;
unsigned char TempMeasTrigger=0;
unsigned char SaltMeasTrigger=0;
unsigned int SaltMeasTime=0;
unsigned char ConductivitySampleNumber=0;
unsigned long ConductivitySum=0;
unsigned int MeanConductivity=0;
unsigned int ConductivityPtoN=0;
unsigned int ConductivityNtoP=0;
signed int SurfaceTemeprature=0;
signed int DeepTemperature=0;
unsigned long MaxSleepTime=0xFFFFFFFF;
unsigned long SleepPeriod=0;
unsigned char SaltMeasRunning=1;
unsigned char XBReset=1;
#endif /*MAIN_H_*/
