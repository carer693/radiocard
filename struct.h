#ifndef STRUCT_H_
#define STRUCT_H_

struct message						// Struct message of data to send from the weatherstation
{
	unsigned int temp[3];
	unsigned long imp[3];
	unsigned int batt[3];
	unsigned int RH;
	unsigned int airtemp;
};
#endif /*STRUCT_H_*/
