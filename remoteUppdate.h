#ifndef REMOTEUPPDATE_H_
#define REMOTEUPPDATE_H_

void FlashEraseInteruptSectors();
void flashEraseMSP();
void flushRecDsubBuf (void);
short int dsubKbhit(void);
unsigned char uppdateFirmware ();
#define DSUB_IN_BUF_SIZE		290

#define Hardware 'A'

short int remoteUppdateFromDsub = 1;
short int waitingForAck=0;
unsigned char memorySpacePlacement=0;		//Indicator for version xA or xB
unsigned int newFirmwareVersion=0;
unsigned int *Flash_pnt;                    // Flash pointer
unsigned long intToAsciiConverted=0;
unsigned int endDsubPnt=0;
unsigned int startDsubPnt=0;
unsigned char recDsubBuf[DSUB_IN_BUF_SIZE];
extern unsigned int RX_pointer_end;
extern unsigned int RX_pointer_start;
extern unsigned char receive[300];

unsigned int systemtime=0;
#endif /*REMOTEUPPDATE_H_*/
