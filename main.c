#include "msp430x24x.h"
#include "intrinsics.h" // Intrinsic functions
#include "stdint.h" // Standard integer types
#include "math.h" // Standard integer types
#include "main.h"
#include "globals.h"
#include "definitions.h"
#include "string.h"

unsigned char start=0;											// Controlls if the start seq. is finished
unsigned char retries=0;										// Help byte to count the nr of retries that took place
unsigned int  ADC_median_result;								// Median result of the ADC and filter function
unsigned char weatherstation_status=0;							// Byte from motherstation that sets different modes in the roadsensor
unsigned char SEND[20] = {0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20};		// Data to be sent buffer
signed char impedance_data_to_calculate[400];					// Total received "RAW" data of the impedance from AD5933
unsigned char data_register_AD5933;								// Received status register of the AD5933
unsigned char slave_address_AD5933=0x0D;						// Address of the AD5933
unsigned char impedance_data[4]={0x00,0x00,0x00,0x00};			// Temp received "RAW" data of impedanse from AD5933
unsigned char dest_address_high[4]={0x00,0x13,0xA2,0x00};		// Address to the mother node, filled when received start meas command
unsigned char dest_address_low[4]={0x00,0x00,0x00,0x00};		//
unsigned char send_data[27];									// API message
unsigned char receive[300];										// Received message
unsigned char RXcount;											// Received nr of received messages
unsigned char API_mode=1;										// API mode ON = 1 OFF = 0
unsigned int BatteryLevel=0;									// Battery level of system
unsigned int RoadTemperature;									// Calculated and mean value of the road temperature
float TemperatureMem;
unsigned long destL;
unsigned long destH=0x0013A200;										// Adress for the mothernode in long
extern unsigned char recDsubBuf[];								// Receive buffer
extern unsigned int endDsubPnt;									// Last byte in received message
extern unsigned int startDsubPnt;								// Start byte position of message

/**
 * Alternativ huvudslinga, anv�nds n�r enheten uppdaterats remote.
 */
#pragma CODE_SECTION(mainx,".flashx")
void mainx(void)
{
}

/**
 * Slinga som k�rs n�r remote update genomf�rs
 */
#pragma CODE_SECTION(applicationProgramB,".flashb")
void applicationProgramB (void)
{
}

/**
 * Huvudslinga som vid uppstart initierar systemet, l�ser inst�llningar fr�n processorns flash och sedan loopar en slinga som g�r allt som ska g�ras.
 */
#pragma CODE_SECTION(main,".flasha")
void main(void)
{
	//TEST
	

	unsigned int i=0;
	unsigned char iAck=0;
	unsigned char firstMeas=0;
	unsigned char OSmessage[8]={0x7E,0x00,0x04,0x08,0x52,0x4F,0x53,0x03};
	WDTCTL = WDTPW+WDTHOLD;                 	// Disable watchdog  ,

  	_EINT();									// Interrupt enabled
  	init();	

  	//P1OUT |= 0x02;
  	//_NOP();
  	// Kom ih�g interruptet f�r tempmeastrigg!!!!!!!!!!!!!
  //	TempMeasTrigger=0;
  	//MeasCardCom('T','4','2');
  //while(1){
  //	MeasCardCom('S','2','1');									// Initialize the road sensor
  	//_NOP();
	////systemtime=0;
	//TBCCTL0 |= CCIE; 							//Enable timer interrupt
	//	do{											// Wait 10s for next measurement
	//		HandleWdt();
	//	}while(systemtime<2000&&!TempMeasTrigger);
  //	MeasCardCom('S','4','1');
  	
  //	systemtime=0;
  //}
  //	TBCCTL0 |= CCIE; 							//Enable timer interrupt
  //	TBCTL =  MC_1+ID_0+TBSSEL_2+TBCLR;
  //	do{
  //		HandleWdt();
 // 	}while(systemtime<2000);
 // 	MeasCardCom('S','4','1');									// Initialize the road sensor
 // 	_NOP();
  	
  	/////
  //P1OUT|=0x04;
  	/*TempMeasTrigger=0;
  	MeasCardCom('S','2','1');
  	systemtime=0;
  	TBCCTL0 |= CCIE; 							//Enable timer interrupt
  	TBCTL =  MC_1+ID_0+TBSSEL_2+TBCLR;
  	do{
  		HandleWdt();
  	}while(systemtime<600);
  	MeasCardCom('S','4','1');
  	TBCCTL0 |= CCIE; 							//Enable timer interrupt
  	TBCTL =  MC_1+ID_0+TBSSEL_2+TBCLR;
  	systemtime=0;
  	do{
  		HandleWdt();
  	}while(systemtime<1400);
  	MeasCardCom('S','4','1');
  	*/
 	///TEST
  	//
 	//P1OUT |= 0x02;
	////
// 	startup_for_pt100();
// 	initiera_AD5933();
// 	impMeasSeq();
	//
	/*ADC12_measurment(1);
					  	P1OUT|=0x01; // Enable 2.048V reference
					  	for(i=1;i<8;i++)
						{
							SEND[i]=0;
						}
					  	ADC12_measurment(1);							// Preformes 19 battery check measurments and stores the mean median value in SEND data
					  	startup_for_pt100();							// Call for the function to start the PT-100 amplification circuit
					  	ADC12_measurment(0);							// Preformes 19 temp. meas. and stores the mean median value in SEND data

					  	P1OUT&=~0x01; //  Disable 2.048V reference
					  	//Not included if the HW doesn't have a surface sensor//
					  	initiera_AD5933();							// Initialize the AD5933
					  	impMeasSeq();	*/
	//
	//SEND[0]=SENSORTYPE;
	HandleWdt();
	start=1;									// Startup seq.
	retries=0;
	startLedFlash();								// Startsequence for led
	//startLedFlash2();
	do											// Wait for start byte "3E" from mothernode
  	{
  		P5OUT|=0x02;
  		start=receive_XBee(17,0,0x90);				// Waiting for receiving start measurment (0x3E) from mother node
  		if(((start==1) && (retries < 30))||start==0xFF){
  			retries++;
  			WdtONOFF(0);							// Put Wdt to hold during LPM4
  			LPM4;									//Set's the MSP430 in LPM4 consuming approx. 0.1uA waiting for external interrupt from XBee
  			WdtONOFF(1);							// Start Wdt
  			HandleWdt();
  		}
  		HandleWdt();
  	_NOP();
  	}while((start==1 || start==0xFF)&& retries < 30 );
 _NOP();

  if(start==0)
  	{
  		for(iAck=0;iAck<5;iAck++)				// Maximum of 5 ACKACK retries
  		{
	  	if(start_ACK()==0)			// Send ACKACK and check if ACKACK has been received by the mother unit
	  		{
		  		_NOP();
		  		// MAIN FUNKTION WHILE LOOP//
		  		while(1){
		  			if(SALT_MEAS_ACTIVE){
		  				SaltMeasRunning=0;
		  				WdtONOFF(0);			// LPM1 f�r fler m�tningar under sovperiod
						TBCTL =  MC_1+ID_0+TBSSEL_1+TBCLR;  // ACLK
						TBCCTL0 |= CCIE; 							//Enable timer interrupt
						LPM3;// Vi har tr�dade sensorer som g�r med trigger p� timer A0, d�rf�r g�r vi endast ner i LPM3
						//SaltMeasTrigger=1;
						TBCTL =  MC_1+ID_0+TBSSEL_2+TBCLR;
		  				WdtONOFF(1);
		  				SaltMeasRunning=1;
		  			}
		  			else{
		  				WdtONOFF(0);			// LPM4 f�r endast tempm�tning vid XBee uppvaknande
		  				LPM4;
		  				WdtONOFF(1);
		  			}
		  			if(SaltMeasTrigger){
		  				//Testmode f�r restsalt
		  				/*systemtime=0;
						TBCCTL0 |= CCIE; 							//Enable timer interrupt
							do{											// Wait 5s
								HandleWdt();
							}while(systemtime<500);
						*/////////////////////
		  				nosaltmeas+=1; //TEST
		  				//P5OUT|=0x06;
		  				ConductivityPtoN=0;
		  				ConductivityNtoP=0;
		  				MeasCardCom('S','2','1');					// Measure positive to neg pulse
		  				if(!TempMeasTrigger){
			  				systemtime=0;
							TBCCTL0 |= CCIE; 							//Enable timer interrupt
								do{											// Wait 10s for next measurement
									HandleWdt();
								}while(systemtime<2000&&!TempMeasTrigger);
	
								if(!TempMeasTrigger){					// No temp meas trigger is set
									TBCCTL0 &= ~CCIE; 					//Disable timer interrupt
									MeasCardCom('S','4','1');			// Measure negative to positive pulse
									if(!TempMeasTrigger){
										ConductivitySampleNumber++;
										ConductivitySum+=ConductivityPtoN+ConductivityNtoP;
										MeanConductivity=(ConductivitySum/ConductivitySampleNumber);
										_NOP();
									}
							}

		  				}
					//P5OUT&=~0x06;
					if(TempMeasTrigger==1){						// Tempm�tning triggad under saltm�tning, st�ng av I2C
						//i2cEndPnt=i2cStartPnt=0; //Reset buffer2
						//i2cStatus=S_NOP;
						//P3SEL &= ~0x06; 
						//UCB0CTL1 |= UCSWRST; //Disable I2C module  
						//IE2 &= ~UCB0TXIE;   // Disable TX interrupt 
						//IE2 &= ~UCB0RXIE;   //Disable RX interupt
						//UCB0I2CIE &= ~UCNACKIE;	//Disable nack interrupt   
						TBCCTL0 |= CCIE;
						systemtime=0;							// Delay f�r att l�ta m�tkortet st�ngas av	10ms
					  	while(systemtime<2); 
					}

		  			}
		  			if(TempMeasTrigger){							// Temperature trigger, dvs trigger fr�n XBee modulen f�r kommunikation
		  				notempmeas++;	//TEST
		  				P1OUT|=0x01; 								// Enable 2.048V reference
		  				DeepTemperature=0;
		  				SurfaceTemeprature=0;

		  				if(SURFACE_TEMP_ACTIVE||ActiveTempMeas==PRIMARY){
		  					MeasCardCom('T','2','2');				// Measure surface temperature
		  				}
		  				if(SENSORTYPE==0x09){			// Delay om b�da RTD ska m�tas.
		  					systemtime=0;
							TBCCTL0 |= CCIE; 							//Enable timer interrupt
								do{											// Wait 10s for next measurement
									HandleWdt();
								}while(systemtime<1);
								TBCCTL0 &=~ CCIE;
		  				}
		  				if(DEEP_TEMP_ACTIVE||ActiveTempMeas==BACKUP){
							MeasCardCom('T','2','3');				// Measure Deep temperature
						}
						if(SaltMeasTime>(SaltMeasInterval-60)){	// Minimum approx 40s between temperature meas and salt meas
		  					SaltMeasTime=SaltMeasInterval-60;
		  					_NOP();
		  				}
						if(SaltMeasInterval!=SALT_WINTER_INTERVAL){	// Om saltm�tningen �r inst�lld p� sommartid ska saltm�tningen g�ras endast en g�ng per vakenperiod. D�rf�r nollas timern f�r saltm�tningen s� den r�knar upp till sommarintervalltiden endast en g�ng per m�tintervall.
							SaltMeasTime=0;
						}

		  				ADC12_measurment(1);						// Preformes 19 battery check measurments and stores the mean median value in SEND data
		  				P1OUT&=~0x01; 								//  Disable 2.048V reference
		  				convert_to_SEND_buffer();
		  				ConductivityReset();						// Reset conductivity meas
		  				convert_to_API_struct();						// Converts the message to API structure
		  				HandleWdt();




		  				weatherstation_status=receive_XBee(17,3,0x90);	// Receive the status of the weatherstation to see if measured data is requested
		  				_NOP();
		  				if(weatherstation_status==1||weatherstation_status==0x11||weatherstation_status==0x21)					// Check for request measure data
		  				{
		  				   while((P2IN & 0x08)==1);					// Wait for Clear To Send from XBee
		  						transmitt_uart(send_data,27);				// Transmitt measure data
		  					    if(receive_XBee(11,1,0x8B)!=0xFF){			// Check if message has been received
		  					    	if(firstMeas==0){						// Yellow led for 2sec repr. first message OK, total init. of system complete
										P5OUT|=0x06;
										one_sec_delay();
										one_sec_delay();
										P5OUT&=~0x06;
										firstMeas=1;
										_NOP();
									}
									P5OUT|=0x02;							// Status, red led when meas. data sent OK and data rec. in motherstation
								}

								if(weatherstation_status==0x11 && (SENSORTYPE==0x11||SENSORTYPE==0x14||BACKUPPT100)){	// Kommando f�r att �ndra PT100 element
									if(ActiveTempMeas==PRIMARY){
										ActiveTempMeas=BACKUP;	
									}
									else if(ActiveTempMeas==BACKUP){
										ActiveTempMeas=PRIMARY;	
									}
									WriteFlashD(ActiveTempMeas,1);		// Skriv aktiva temperatursensor till minne
								}
								if(DeepTemperature>60 && SurfaceTemeprature==32767 || SurfaceTemeprature>60 && DeepTemperature==32767){	// Hysteres f�r omslag av sommartidsm�tning av saltm�tare
									TempAbove6++;
									if(TempAbove6==255){
										TempAbove6=9;
									}
								}
								else{
									TempAbove6=0;
								}

								if((weatherstation_status==0x21 || (TempAbove6==8||(TempAbove6==0&&(SaltMeasInterval!=SALT_WINTER_INTERVAL)))) && SALT_MEAS_ACTIVE){	// Kommando f�r att �ndra tid mellan saltm�tningar, antingen via remote kommando eller automatiskt vid tempniv�er
									transmitt_uart(OSmessage,12);					// Send AT command for Operating Sleeptime
									receive_XBee(13,6,0x88);
									if(SaltMeasInterval==SALT_WINTER_INTERVAL&&OperatingSleeptime>27000){									// M�ste var �ver 4 min sleeptime f�r att oofseten med 2 min inte ska g�ra s� att mer �n 1 m�tning per intervall g�rs
										if(SurfaceTemeprature>0&&DeepTemperature==32767||DeepTemperature>0&&SurfaceTemeprature==32767){		// Temperatur �ver 0C, 12kHz klocka ger ca 500ms f�r 5000 pulser
											SaltMeasInterval=((OperatingSleeptime-12000)/50);												// Ber�kna tidpunkt f�r saltm�tning. Ca 2min innan temperaturm�tningen g�rs ska saltm�tningen g�ras
										}
										else if(SurfaceTemeprature<=0&&DeepTemperature==32767||DeepTemperature<=0&&SurfaceTemeprature==32767){		// Temperatur under 0C, 12KHz klocka ger ca 540ms f�r 5000 pulser (G�ller ej d� vi anv�nder temperaturen f�r att styra tid mellan salt iom att vintertiden s�tts under 6C)
											SaltMeasInterval=((OperatingSleeptime-12000)/54);														// Ber�kna tidpunkt f�r saltm�tning. Ca 2min innan temperaturm�tningen g�rs ska saltm�tningen g�ras
										}


									}
									else if(SaltMeasInterval!=SALT_WINTER_INTERVAL){	// Inte inst�lld p� vintertidsm�tning
										SaltMeasInterval=SALT_WINTER_INTERVAL;
									}
									SaltMeasTime=0;
								}
								for(i=0;i<3;i++){
									time_delay(65535);
								}
								P5OUT&=~0x02;
								if(XBReset==0){
								XBReset=1;}
								//Testmode f�r restsalt
								//SaltMeasTime=SaltMeasInterval;
								////////////


		  				}
						else if(weatherstation_status==2){				// Check if remote update status has been sent from the motherunit
							remoteUpdate();

						}
						else if (weatherstation_status==3){			// Reset roadsensor
							P1OUT|=0x04;							// Reset
							for(i=0;i<70;i++)						// Hold XBee reset for 4.6s to wait for the mother node to initialize
							{
								time_delay(65535);
								if(i%30==0)
								{
									HandleWdt();
								}
							}
							FCTL1 = 0;								// Restart
						}
						else if (weatherstation_status=='>'){		// Used to be able to reset the motherunit without reset the sensor because the motherunit waits for ACKACK from the sensors after a reset when sending >
							start_ACK();
						}
		  				if(SALT_MEAS_ACTIVE&&firstMeas==1&&XBReset>0){						// Inf�rande av maximal sovtid innan reset f�r saltsensorbestyckade vbs. ESD test.
							transmitt_uart(OSmessage,12);						// Send AT command for Operating Sleeptime
							receive_XBee(13,6,0x88);
							if(OperatingSleeptime>250){							// Vid reset �r default OS 200. Vill ej l�gga in defaultv�rde utan faktisk sovperiod
								MaxSleepTime=(OperatingSleeptime/100)*4;		// MaxSleepTimes j�mf�relse r�knas upp med ca 0.5s/cykel. OS �r i 10ms, tar sedan dubbla den tiden f�r att f� max sovtid
								XBReset=2;
							}
							SleepPeriod=0;				// Nolla r�knaren f�r �vervakning
						}
		  			}
		  			//TEST
		  			if(SaltMeasTrigger==1&&TempMeasTrigger==1){
		  				_NOP();
		  			}
		  			if(SaltMeasTrigger==1){							// Nolla r�knaren f�r n�sta saltm�tning
		  				SaltMeasTime=0;
		  			}
		  			SaltMeasTrigger=0;								// Nolla triggers
		  			TempMeasTrigger=0;

		}
					  	_NOP();
	}
			 	WdtONOFF(0);
			 	LPM4;
			 	WdtONOFF(1);
}

	FCTL1 = 0;	//Restart if not ACKACK received by the motherunit after 5 tries
	}
	FCTL1 = 0;	//Restart if Start measurment '>' from mothernode hasn't appeard adter 30 tries
 }

unsigned char MeasCardCom(unsigned char type, unsigned char n_wires, unsigned char port){
						unsigned char okmeas=0;

						P1OUT |= 0x02;											// Start the LTC3200-5.					  	
					  	TBCCTL0 = CCIE;						// Interrupt enable - systemtime to be able to use the while loops as wait functions for the HW-I2C
					  	systemtime=0;					
					  	while(systemtime<2);
					  	//ADC12_measurment(1); //Battery measurement

	  					HandleWdt();//Init systemtime
					  	startI2C();
					  	//K//startup_for_pt100();							// Call for the function to start the PT-100 amplification circuit
					  	//K//ADC12_measurment(0);							// Preformes 19 temp. meas. and stores the mean median value in SEND data
					  	systemtime=0;
					  	
					  	while((!i2cInitOk) && systemtime<50 && (!TempMeasTrigger||type=='T'))//Wait for communication or Error
  						{
  							HandleWdt();
  						}
  						if(systemtime>=50||(TempMeasTrigger&&type!='T')) //Error
  						{
  							//TODO?
  							_NOP();
  							shdnMeasCard();// ??
  							return 0;
  						}

  						//sendI2CData('Q','S','2','1',0x11); //Query data from Salt sensor on channel 1 with two wires. more data
  						sendI2CData('Q',type,n_wires,port,0x10);
  						//sendI2CData('Q','T','2','2',0x00); //Query data from Temp sensor on channel 2 with two wires. No more data
  						//sendI2CData('Q','T','4','3',0,0); //Query data from Temp sensor on channel 3 with four wires.No more data
  						systemtime=0;

				  		while((!i2cDataReceived) && systemtime<120 && (!TempMeasTrigger||type=='T')) //Wait for data
				  		{
				  			HandleWdt();
				  		}
  						if(systemtime>=120 || (TempMeasTrigger&&type!='T')) //Error
  						{
  							//TODO?
  							shdnMeasCard();// ??
  							return 0;
  						}
  						okmeas=I2C_data_extract();							// Extract all measurements from data
				    	_NOP(); //Breakpoint here
				    	stopI2C(); //Stop communication
				    	systemtime=0;
				    	while((!i2cStopOk)&&systemtime<50 && (!TempMeasTrigger||type=='T')) //Wait for data
				  		{
				  			HandleWdt();
				  		}
  						if(systemtime>=50 || (TempMeasTrigger&&!type=='T')) //Error
  						{
  							//TODO?
  							shdnMeasCard();// ??
  							return 0;
  						}
  						//K//Disable MSP on measurement board
  						//P3REN&=~0x06;						// Pull up I2C disable
  						//P3OUT&=~0x06;
  						TBCCTL0 &= ~CCIE; //Disable timer interrupt
				  		_NOP();


					  	//Not included if the HW doesn't have a surface sensor//
					  	//K//initiera_AD5933();							// Initialize the AD5933
					  	//K//impMeasSeq();
					  	////////////////////////////////////////////////////////
						//Put values into SEND

						shdnMeasCard();
						_NOP();
}

//Pin interrupt for exit Low Power Mode when the measuring cycle interval has been reached.
//The XBee module sets ON/SLEEP high and that triggers the pin interrupt on MSP430 that wakes up from LPM4

#pragma vector = PORT2_VECTOR
__interrupt void PORT2_ISR(void)
{
  do
  	{
		P2IFG = 0; 							// Clear interrupt on P2
	}while ((P2IFG != 0));
   	TempMeasTrigger=1;
	LPM4_EXIT;			// Exit LPM4 to start the CPU
}

// Disables the TIMERA0 interrut
#pragma vector = TIMERA0_VECTOR
__interrupt void TIMERA0(void)
{
_NOP();
}
// Disables the TIMERA1 interrut
#pragma vector = TIMERA1_VECTOR
__interrupt void TIMERA1(void)
{
_NOP();
}

#pragma vector = TIMERB0_VECTOR
__interrupt void TIMERB0(void)
{
	/*if(systemtime%200==0)
	{
		TempClk++;
	}*/
	P6OUT^=0x20;	//TEST
	TBCCR0 = 5000;
	systemtime++;
	if(!TempMeasTrigger){
		SaltMeasTime++;
	}
	if(systemtime) //5ms
	{
		i2cHandler();
	}
	if (systemtime == 60000)
	{
		systemtime = 0;
	}
	if(SaltMeasTime>=SaltMeasInterval && SALT_MEAS_ACTIVE){
		SaltMeasTrigger=1;
		LPM3_EXIT;
	}
	if(SALT_MEAS_ACTIVE&&SaltMeasRunning==0&&TempMeasTrigger==0&&XBReset==2){
		SleepPeriod++;
		if(SleepPeriod>MaxSleepTime){
			//FCTL1=0; //Restart
			initXbee();
			XBReset=0;
			SleepPeriod=0;
		}
	}
	TBCCTL0 &= ~CCIFG;		// Reset timer
}


#pragma vector=NMI_VECTOR
__interrupt void nmi (void)
{
  IFG1 &= ~NMIIFG;                          // Reclear NMI flag in case bounce
  IE1 |= NMIIE;                             // Enable NMI
}




















