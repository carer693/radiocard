/*
 * functions.h
 *
 *  Created on: 21 nov 2014
 *      Author: CJ
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
void time_delay(unsigned int delaytime_us);
unsigned int status_register_poll(unsigned char status_register, unsigned char poll_mask);
void transmitt_data(unsigned char data[], unsigned char number_of_bytes);
void receive_data(unsigned char register_address, unsigned char number_of_bytes);
unsigned int AT_prog(unsigned long interval, unsigned int command);

unsigned int wdt=0;
unsigned long OperatingSleeptime=0;

extern unsigned int MeanConductivity;
extern unsigned int systemtime;
extern unsigned int ConductivityPtoN;
extern unsigned int ConductivityNtoP;
extern signed int SurfaceTemeprature;
extern signed int DeepTemperature;
extern unsigned char ConductivitySampleNumber;
extern unsigned long ConductivitySum;
#endif /* FUNCTIONS_H_ */
