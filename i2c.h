#ifndef I2C_H_
#define I2C_H_

//i2cStatus define
#define S_NOP 0x00
#define D_START 0x01
#define D_RECEIVE 0x02
#define D_CRC_FAIL 0x03
#define D_SENDING 0x04
#define D_RECEIVED 0x05

void createI2CMessage (unsigned char type, unsigned char sensor, unsigned char wires, unsigned char channel, unsigned char status_c);
//void sendI2CData (unsigned char type, unsigned char sensor, unsigned char wires, unsigned char channel, unsigned char eraseBuffer, unsigned char moreDataToBeSent);



char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };


unsigned char i2cBuffer[100];
unsigned char i2cBufferOut[100];
unsigned char i2cEndPntOut=0;
unsigned char i2cStartPntOut=0;
unsigned char i2cEndPnt=0;
unsigned char i2cStartPnt=0;
unsigned char i2cStatus=0;
unsigned char i2cInitOk=0;
unsigned char i2cDataReceived=0;
unsigned char i2cStopOk=0;
extern unsigned char SEND[20];
extern unsigned int ConductivityPtoN;
extern unsigned int ConductivityNtoP;
extern signed int SurfaceTemeprature;
extern signed int DeepTemperature;
#endif /*I2C_H_*/
