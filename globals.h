#ifndef GLOBALS_H_
#define GLOBALS_H_

extern unsigned long destL, destH;
extern unsigned int  ADC_median_result;
extern unsigned char SEND[20];
extern signed char impedance_data_to_calculate[400];
extern unsigned char data_register_AD5933;
extern unsigned char slave_address_AD5933;
extern unsigned char impedance_data[4];
extern unsigned char dest_address_high[4];
extern unsigned char dest_address_low[4];
extern unsigned char send_data[27];
extern unsigned char receive[300];
extern unsigned char RXcount;
extern unsigned char API_mode;
extern unsigned int newFirmwareVersion;
extern unsigned int endDsubPnt;
extern unsigned int startDsubPnt;
extern unsigned int BatteryLevel;
extern unsigned int RoadTemperature;
extern float TemperatureMem;
extern unsigned char recDsubBuf[];
extern long impedance_value[];
extern unsigned long intToAsciiConverted;
#endif /*GLOBALS_H_*/
