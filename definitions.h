#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#define CurrentFirmwareVersion 30

#define SALT_WINTER_INTERVAL 240     //123 // 60/0.416667  (VLOCLK = 12kHz, timer count up 5000) Justerade f�r ca 2min vid rumstemp
#define SALT_SUMMER_INTERVAL 1300	// 14min intervall mellan saltm�tningarna f�r sommardrift
#define CALIB 1
#define SENSORTYPE	0x14			// Sensortyp
#define BACKUPPT100 0				// Define f�r om sensortyp �r 0x01 men backup �r tillg�nglig och FW i XBP ej direkt st�der
#define PRIMARY 1
#define BACKUP 2
//	Sensortyp
#if SENSORTYPE==1	&& BACKUPPT100!=1					// Roadsensor
			#define SALT_MEAS_ACTIVE 0
			#define SURFACE_TEMP_ACTIVE 1
			#define DEEP_TEMP_ACTIVE 0
	#endif
#if SENSORTYPE==1	&& BACKUPPT100==1
			#define SALT_MEAS_ACTIVE 0
			#define SURFACE_TEMP_ACTIVE 0
			#define DEEP_TEMP_ACTIVE 0
	#endif

#if SENSORTYPE==4					// Salt och yttemp
			#define SALT_MEAS_ACTIVE 1
			#define SURFACE_TEMP_ACTIVE 1
			#define DEEP_TEMP_ACTIVE 0
	#endif
#if SENSORTYPE==5						// Yt, Djup och Saltsensor
			#define SALT_MEAS_ACTIVE 1
			#define SURFACE_TEMP_ACTIVE 1
			#define DEEP_TEMP_ACTIVE 1
	#endif
#if SENSORTYPE==6						// Djup och saltsensor
			#define SALT_MEAS_ACTIVE 1
			#define SURFACE_TEMP_ACTIVE 0
			#define DEEP_TEMP_ACTIVE 1
	#endif
#if SENSORTYPE==7						// Djupsensor
			#define SALT_MEAS_ACTIVE 0
			#define SURFACE_TEMP_ACTIVE 0
			#define DEEP_TEMP_ACTIVE 1
	#endif
#if SENSORTYPE==8						// Saltsensor
		#define SALT_MEAS_ACTIVE 1	
		#define SURFACE_TEMP_ACTIVE 0
		#define DEEP_TEMP_ACTIVE 0
	#endif
#if SENSORTYPE==9						// Yt och djupsensor
		#define SALT_MEAS_ACTIVE 0
		#define SURFACE_TEMP_ACTIVE 1
		#define DEEP_TEMP_ACTIVE 1
	#endif
#if SENSORTYPE==0x11					// Roadsensor med backup ist�llet f�r djupm�tning
		#define SALT_MEAS_ACTIVE 0
		#define SURFACE_TEMP_ACTIVE 0
		#define DEEP_TEMP_ACTIVE 0
	#endif
#if SENSORTYPE==0x14					// Roadsensor med backup och saltm�tning
		#define SALT_MEAS_ACTIVE 1
		#define SURFACE_TEMP_ACTIVE 0
		#define DEEP_TEMP_ACTIVE 0
	#endif


#define SLEEPMODE			((unsigned int)(0x534D))
#define SLEEPOPTION			((unsigned int)(0x534F))
#define	WAKEDELAY			((unsigned int)(0x5748))
#define ONE_SEC				((unsigned long)(0x3E8))
#define TWOHALF_SEC			((unsigned long)(0x9C4))
#define HALF_SEC			((unsigned long)(0x1F4))

#define TWO_SEC_S			((unsigned long)(0xC8))
#define ELEVEN_MIN_W		((unsigned long)(0xA1220))
#define TRETTI_S_W			((unsigned long)(0x7530))
#define NR_HOPS				((unsigned long)(0x03))
#define GT_TIME				((unsigned long)(0x02))
#define FOUR				((unsigned long)(0x04))
#define ND_TIME				((unsigned long)(0x20))

#define TEN_SEC				((unsigned long)(0x2710))
#define ONE_MINUTE_SLEEP	((unsigned long)(0x1770))
#define THIRTY_SEC_SLEEP	((unsigned long)(0xBB8))
#define TWENTY_SEC_SLEEP	((unsigned long)(0x7D0))
#define TEN_SEC_SLEEP		((unsigned long)(0x3E8))
#define TWO_SEC_SLEEP		((unsigned long)(0xC8))
#define ONEHALF_SEC_SLEEP	((unsigned long)(0x96))
#define TWENTY_SEC			((unsigned long)(0x4E20))
#define HALF_SEC			((unsigned long)(0x1F4))
#define ONE_SEC_SLEEP		((unsigned long)(0x64))
#define TEN_MIN				((unsigned long)(0x927C0))

#define SIXTY_SEC			((unsigned long)(0xEA60))
#define SLEEP_TIME_MEASURE	((unsigned long)(0x3E8))
#define TEN_MINUTES_SLEEP	((unsigned long)(0xEA60))
#define ONE_HOUR_SLEEP		((unsigned long)(0x57E40))
#define RESET				((unsigned long)(0x00))

#define SET_ONE				((unsigned long)(0x01))
#define SET_BIT_ONE			((unsigned long)(0x02))
#define SET_BIT_ONE_THREE	((unsigned long)(0x0A))
#define SET_BIT_TWO			((unsigned long)(0x04))
#define SEVEN				((unsigned long)(0x07))
#define OPCHE				((unsigned long)(0x0E))
#define OPCHC				((unsigned long)(0x0C))

#define WAKETIME			((unsigned int)(0x5354))
#define BAUDRATE			((unsigned int)(0x4244))
#define	SLEEPTIME 			((unsigned int)(0x5350))
#define WAKEDELAY			((unsigned int)(0x5748))
#define API					((unsigned int)(0x4150))
#define NODEDISC			((unsigned int)(0x4E44))
#define DESTLOW				((unsigned int)(0x444C))
#define DESTHIGH			((unsigned int)(0x4448))
#define NETWORKHOPS			((unsigned int)(0x4E48))
#define GUARDTIME			((unsigned int)(0x4754))
#define Digi0  				((unsigned int)(0x4430))	
#define Digi11  			((unsigned int)(0x5031))
#define DIGI6				((unsigned int)(0x4436))
#define NDTIMEOUT  			((unsigned int)(0x4E54))	
#define CHANNEL				((unsigned int)(0x4348))
#define WRITE				((unsigned int)(0x5752))

#define ZERO		((unsigned long)(0x00))
#define ONE			((unsigned long)(0x01))
#define EIGHT		((unsigned long)(0x08))
#define APION		((unsigned long)(0x01))
#define APIOFF		((unsigned long)(0x00))
#define API			((unsigned int)(0x4150))



#define INFOD_MEM	0x1000 //64byte
#define INFOC_MEM	0x1040 //64byte
#define INFOB_MEM	0x1080 //64byte
#define INFOA_MEM	0x10C0 //Reserved (Locked as default)

// Memory for active PT100 sensor
#define ACTIVE_PT100					(*(char*)(INFOD_MEM+0))
#define M_CHANNEL						(*(char*)(INFOB_MEM+0))
// Calibration data

#define BelowZeroDerivata				(*(char*)(INFOC_MEM+0))
#define AboveZeroDerivata				(*(char*)(INFOC_MEM+1))
#define BelowZeroOffset					(*(char*)(INFOC_MEM+2))
#define AboveZeroOffset					(*(char*)(INFOC_MEM+3))
#define A2_1							(*(char*)(INFOC_MEM+4))
#define	A2_2							(*(char*)(INFOC_MEM+5))
#define A2_3							(*(char*)(INFOC_MEM+6))
#define	A1_1							(*(char*)(INFOC_MEM+7))
#define A1_2							(*(char*)(INFOC_MEM+8))
#define	A1_3							(*(char*)(INFOC_MEM+9))
#define A0_1							(*(char*)(INFOC_MEM+10))
#define	A0_2							(*(char*)(INFOC_MEM+11))
#define A0_3							(*(char*)(INFOC_MEM+12))
#define B1_1							(*(char*)(INFOC_MEM+13))
#define	B1_2							(*(char*)(INFOC_MEM+14))
#define B1_3							(*(char*)(INFOC_MEM+15))
#define B0_1							(*(char*)(INFOC_MEM+16))
#define	B0_2							(*(char*)(INFOC_MEM+17))
#define B0_3							(*(char*)(INFOC_MEM+18))

#endif /*DEFINITIONS_H_*/
