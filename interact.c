#include <msp430x24x.h>	
#include "definitions.h"
//	
//	
//	
///**
// * Writes one (1) byte (8 bit) into MSP-flash memory
// * @param Data_ptr memory adress
// * @param byte data to write
//*/

void FlashWb( char *Data_ptr, char byte ) {
	FCTL3 = 0x0A500;											// Lock = 0
	FCTL1 = 0x0A540;											// WRT = 1
	*Data_ptr=byte;												// program Flash byte
	FCTL1 = 0x0A500;											// WRT = 0
	FCTL3 = 0x0A510;											// Lock = 1
} // Flash_wb()

/**
 * Erase one (1) segment of MSP-flash memory
 * @param Data_ptr memory adress
*/

void FlashClr( int *Data_ptr ) {
	FCTL3 = 0x0A500;											// Lock = 0
	FCTL1 = 0x0A502;											// ERASE = 1
	*Data_ptr=0;												// erase Flash segment
	FCTL1 = 0x0A500;											// ERASE = 0
	FCTL3 = 0x0A510;											// Lock = 1
} // Flash_clr()

void WriteFlashD(unsigned char app_data,unsigned char nr_of_bytes)
	{


		char* ptr;
			int i;

			unsigned char j=0;
			HandleWdt();
			_DINT();																			// Disable interrupts
			IE1 &= ~(NMIIE);
			FlashClr( (int *) INFOD_MEM );				// erase segment D
			ptr = (char *) INFOD_MEM;					// Set pointer
			HandleWdt();
			FlashWb( ptr++, app_data);
			_EINT();
			IE1 |= NMIIE;																// Enable interrupt


			//updatestate = 11;
			//updatemode = 20;															// 2 sek till f�r att hantera fler kommandon

	}

//
//void externrec_data_controll()
//{
//	if(!strncmp(&receive[0],"FyMA=", 5))
//	{
//		char* ptr;
//			int i;
//			_DINT();																			// Disable interrupts
//			IE1 &= ~(NMIIE);
//			Flash_clr( (int *) INFOD_MEM );				// erase segment D
//			ptr = (char *) INFOD_MEM;						// Set pointer
//			for (i=0;i<31;i++) 
//			{
//				Flash_wb( ptr++, receive[5+i]);
//			}
//			_EINT();
//			IE1 |= NMIIE;																// Enable interrupt			
//			for(i=0;i<100;i++)					// Reset received bytes buffer
//			{
//				receive[i]=0;
//			}
//		
//			//updatestate = 11;
//			//updatemode = 20;															// 2 sek till f�r att hantera fler kommandon
//		}
//		else
//		{
//			for(i=0;i<100;i++)					// Reset received bytes buffer
//			{
//				receive[i]=0;
//			}
//			//updatestate=1;
//			//updatemode=0;
//		}
//}
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
