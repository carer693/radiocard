#ifndef INIT_H_
#define INIT_H_

void initTimer();
void initUart();
void initXbee();
void initClk();
void HandleWdt();

extern unsigned int DeepTemperature;
extern unsigned char ActiveTempMeas;
extern unsigned char receive[20];
extern signed char impedance_data_to_calculate[400];
extern unsigned char API_mode;
extern signed int SurfaceTemeprature;
unsigned char powerON;
#endif /*INIT_H_*/
