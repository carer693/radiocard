///**
// * \file remoteUppdate.c
// * \brief Remote update
// * 
// * Inneh�ller funktioner f�r nerladdning av ny mjukvara.
// */
//
//#include  <msp430x24x.h>
//
//#include "remoteUppdate.h"
//
///**
// * Verifiering av nerladdad sektor.
// */
//short int programVerify (unsigned int Cr, unsigned char Memtype, unsigned int size)
//{
//	unsigned int *pnt;                        // Flash pointer
//	unsigned int i, checksum=0;
//	if(Memtype == 'A')						//Verify Sector A
//	{
//		pnt = (unsigned int *) 0x1200;		//Start address for SectorA
//		for(i=0x1200; i<(size +32+ 0x1200); i=i+2) 
//		{
//			checksum ^= *pnt++;
//		}		
//	}
//	else									//Verify Sector B
//	{
//		pnt = (unsigned int *) 0x8800;		//Start address for sector B
//		for(i=0x8800; i<(size + 32+ 0x8800); i=i+2) 
//		{
//			checksum ^= *pnt++;
//		}	
//	}
//	if(Cr == checksum) 	//Verify correct?
//	{
//		return 1;
//	}
//	return 0;
//}
//
///**
// * Funktion som verifierar att data som l�st in via RS232 eller GPRS �r korrekt. All data mellan < och * summeras med XOR och verifieras med mottaget crc summa. Funktionen returnerar 1 f�r r�tt data och 0 f�r korrupt data.
// */
// 
//
// 
//short int checkRemoteUppdateCrc (void)
//{
//	unsigned int i, byte; 
//	unsigned char crc;
//	i=0;
//	if(!remoteUppdateFromDsub) 	
//	{
//    	i = (strstr(&recDsubBuf[0], ">RTF"));		//Find message
//	}
//	i=i+13;
//	HandleWdt();					//Toggle WDT
//	MPY = 100;							//Check the sum of bytes in message. It is in ASCII
//   	OP2 = (recDsubBuf[13]&~(0x30));	
//    byte = RESLO;
//    MPY = 10;      				
// 	OP2 = (recDsubBuf[14]&~(0x30));	
//    byte += RESLO;
//  	byte += (recDsubBuf[15]&~(0x30));
//	i = 0;
//	i++;	//Skip '>'
//	if(i == DSUB_IN_BUF_SIZE)	
//	{
//		i=0;
//	}
//	crc = recDsubBuf[i];					//Get firs byte 'R'
//	i++;
//	if(i == DSUB_IN_BUF_SIZE)
//	{
//		i=0;
//	}						
//	for (i; i<(byte + 22); i++) 	//Do between < and *
//	{
//		crc^=recDsubBuf[i];					//XOR data
//		if(i == DSUB_IN_BUF_SIZE)
//		{
//			i=0;
//			
//		}		
//	}
//	if(crc == recDsubBuf[i+1])	//Check if same crc
//	{
//		return 1;
//	}
//	flushRecDsubBuf();						//Flush the bufferdata	
//	return 0;	
//}
//
///**
// * Radering av MSP innan ny data l�ggs in
// */
//void flashEraseMSP (void)
//{
//	unsigned int sector;
//	unsigned int *activeSector;
//	HandleWdt();					//Toggle WDT
//	_DINT();							//Disable interrupt
//	activeSector = (unsigned int *) 0xFE00;	//Get the address of active sector
//	if(*activeSector == 0x4141)
//	{
//		for(sector=0x8800; sector<0xFE00; sector = sector+512) //Earse sector B
//		{
//			FCTL1 = FWKEY + ERASE;                    // Set Erase bit
//  			FCTL3 = FWKEY;                            // Clear Lock bit
//  			activeSector = (unsigned int *) sector;
//  			*activeSector = 0;
//  			HandleWdt();
//		}
//	}
//	else
//	{
//		for(sector=0x1200; sector<0x8800; sector = sector+512) //Erase sector A
//		{
//			FCTL1 = FWKEY + ERASE;                    // Set Erase bit
//  			FCTL3 = FWKEY;                            // Clear Lock bit
//  			activeSector = (unsigned int *) sector;
//  			*activeSector = 0;
//  			HandleWdt();
//		}		
//	}
//	_EINT();	//Enable interrupt
//}
//
///**
// * Funktion f�r att skapa meddelande till server eller PC program. Om uppdatering sker via gprs skapas At-kommandon till modemet. Funktionen genererar en str�ng som inneh�ller hela protokollet med clientID, gprsAckID, QTF-data, CRC m.m
// * Sker uppdateringen remote skickas str�ngen till GPRS modemet, annars till RS232 uart f�r PC-uppdatering. 
// */
//void createQueryMessage (char Hw, unsigned int fw, unsigned char memPlace, unsigned int segNr, short Ack)
//{
//	unsigned int i=0;
//	flushRecDsubBuf();	
//	if(!Ack)		//Add the rest of data before sending
//	{
//		recDsubBuf[endDsubPnt++] = '>';
//		recDsubBuf[endDsubPnt++] = 'Q';
//		recDsubBuf[endDsubPnt++] = 'T';
//		recDsubBuf[endDsubPnt++] = 'F';
//		if(segNr == 0xFFFF)				//Are vi sending the firs query message?
//		{
//			recDsubBuf[endDsubPnt++] = 'I';
//		}
//		else
//		{
//			recDsubBuf[endDsubPnt++] = 'W';		
//		}
//		recDsubBuf[endDsubPnt++] = Hw;		//Hardware 'B' for Bluetex
//		recDsubBuf[endDsubPnt++] = fw>>8;		//MSB of firmware
//		recDsubBuf[endDsubPnt++] = fw;		//LSB of firmware
//		recDsubBuf[endDsubPnt++] = memPlace;	//Memoryplacement. A or B
//		if(segNr != 0xFFFF)
//		{
//			recDsubBuf[endDsubPnt++] = 0x00;	//Segment number
//			recDsubBuf[endDsubPnt++] = 0x00;	
//			recDsubBuf[endDsubPnt++] = segNr>>8;	
//			recDsubBuf[endDsubPnt++] = segNr;		
//		}	
//		recDsubBuf[endDsubPnt++] = '<';		//End of message
//		recDsubBuf[endDsubPnt++] = 0x0D;
//		recDsubBuf[endDsubPnt++] = 0x0A;
//	}
//	startDsubPnt=0;
//	if (remoteUppdateFromDsub)	
//	{
//		HandleWdt();					//Toggle WDT
//		UCA0TXBUF = recDsubBuf[0];
//		for (i=1; i<endDsubPnt;)
//		{
//			if ((IFG2&UCA0TXIFG)) 		//Buffer ready to send character?
//			{																				 																				// Aktivera s�ndning p� UART0
//				UCA0TXBUF = recDsubBuf[i];
//				i++;
//			}
//		}
//		flushRecDsubBuf();
//		HandleWdt();					//Toggle WDT
//	}					
//}
//
///**
// * Funktion som kollar efter svar fr�n PC program eller server. Om uppdatering sker lokat letar funktionen efter <CRLF f�r signalering att nytt meddelande mottagits. Sker uppdateringen remote hanterar funktionen alla n�dv�ndiga AT kommandon till modemet f�r utl�sning av data.
// * Funktionen tar hand om ack fr�n servern automatiskt och returnerar 1 endast n�r ny mjukvarudata nerladdats. Funktionen uppdaterar gprsAckID och gprsMessageID.
// */
//short int checkResponseMessage(void)
//{
//	if(remoteUppdateFromDsub)	
//	{
//		if (dsubKbhit())
//		{
//			if(recDsubBuf[endDsubPnt-3] == '<' && recDsubBuf[endDsubPnt-2] == 0x0D && recDsubBuf[endDsubPnt-1] == 0x0A  )	//Last byte carrage return?
//			{
//				return 1;
//			}
//		}
//		return 0;
//	}
//}
//
///**
// * Huvudfunktion f�r nerladdning av mjukvara. Funktionen avg�r om uppdatering sker lokalt eller remote och st�nger ner alla on�diga interrupt och timrar som inte beh�vs f�r uppdateringen. 
// * Funktionen kontrollerar att mjukvaran som laddas ner tillh�r r�tt h�rdvara och minnesplats och skriver ner hela mjukvaran till eeprom. 
// * Efter nerladdning skriver funktionen 0xF0 p� adress 0x00 i eeprom f�r att signalera vid uppstart att ny mjukvara laddats ner.
// */
//void uppdateFirmware (void)
//{
//	unsigned int *firmwarePlacement;			//Pointer to segment placement
//	unsigned int *Flash_pnt;                    // Flash pointer
//	unsigned int *Interrupt_pnt;				//Pointer to where the interrupt vector is saved
//	unsigned long int fileSize=0;				//Size of firmware to download
//	unsigned int crc=0;
//	unsigned int segmentNr=0, numberOfSegments=0;
//	unsigned int i, j, k;
//	unsigned long int temp;
//	short int moreDataToDownload=0;				//Indicator that tells us if we have downloaded all data
//	unsigned char numberOfRetries=0;
//	systemtime=0;
//	_DINT();
//	FCTL2 = FWKEY + FSSEL_1 + FN4;          	// Flash Timing Generator ~460kHz
//	firmwarePlacement = (unsigned int *) 0xFE00;	//0xFE00 is the address where active sector variable is
//	//OBS! DETTA SKA EJ VARA MED I SENARE VERSIONER: ENDAST F�RSTA!!!!!
//	FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
//	FCTL3 = FWKEY;                     // Clear Lock bit
//	*firmwarePlacement = 0x4141;
//	FCTL1 = FWKEY;            // Clear WRT bit
//  	FCTL3 = FWKEY + LOCK;     // Set LOCK bit
//  	
//  	/////////////////////////////////////////////////////////////////////
//	_EINT();
//	if(*firmwarePlacement == 0x4141)	//Are we running in sector A?
//	{
//		memorySpacePlacement = 'B'; 	//Sector B is not used and will be used for the new firmware
//		Flash_pnt = (unsigned int *) 0x8800;	//Address of sector B
//	}
//	else
//	{
//		memorySpacePlacement = 'A'; 	//Sector A is not used and will be used for the new firmware
//		Flash_pnt = (unsigned int *) 0x1200;	//Address of sector A
//	}
//	HandleWdt();					//Toggle WDT
//	//L�gg in alla
//	//ME1 &= ~(UTXE0 + URXE0);		// Disable UART0 TXD/RXD
//	IE2 |= UCA0RXIE;				//Enable Uart rx interrupt	
//	TACCTL0 = 0x00;					//Stop timer A
////	TBCCTL0 =0x00;					//Stop timer B
///*	TACTL = TASSEL_2 + ID_0 + MC_0; // SMCLK, SMCLK div1, STOP, NO interrupt (Timer A)
//	TACCTL0 = SCS; 					//Synchronize capture source
//	TACCTL0 = 0;
//	CCR0+=40000;				//Time to next bit
//	TACTL |= MC_2; 					// Start timer A in continius mode*/
//	IE2 &= ~(UCA0TXIE);	
//	HandleWdt();
//	createQueryMessage(Hardware, newFirmwareVersion, memorySpacePlacement, 0xFFFF, 0);	//Create firs message and send
//	while(1)	//Get softwareinfo
//	{
//		HandleWdt();
//		if (checkResponseMessage())		
//		{
//			i=5;    //Skip >RTFI	�
//			systemtime=0;
//			HandleWdt();
//			if(recDsubBuf[i] == Hardware)	//Right hardware? B for BlueTex
//			{
//				temp = recDsubBuf[++i]<<8;	//Msb of version
//				temp +=recDsubBuf[++i];		//Lsb of version
//				if(temp == newFirmwareVersion)	//Same version?
//				{
//					if(recDsubBuf[++i] == memorySpacePlacement) //Right memoryplacement?
//					{
//						i++;	//Skip the 16 MSB of program Size
//						i++;
//						fileSize = recDsubBuf[++i]<<8;
//						fileSize += recDsubBuf[++i];
//						for(j=512; j<fileSize; j=j+256)	//Calculate the number of segments to get
//						{
//							numberOfSegments++;	// segments � 256
//						}
//						if(fileSize%256)
//						{
//							numberOfSegments++; //Rest of data
//						}
//						numberOfSegments++; //Interruptvector. The last segment of data
//						HandleWdt();
//						flashEraseMSP();	//Erase the new memoryplacement
//						createQueryMessage(Hardware, newFirmwareVersion, memorySpacePlacement, segmentNr, 0);	//Get the first segment of data
//						break;	//Go to next loop
//					}
//					else
//					{
//						//wrong memoryplacement
//						FCTL1 = 0;	//Restart 
//					}
//				}
//				else
//				{
//					//Wrong firmware
//					FCTL1 = 0;	//Restart 
//				}
//			}
//			else
//			{
//				//wrong hardware
//				FCTL1 = 0;	//Restart 
//			}
//		}
//		if(systemtime >=12000)
//		{
//			//Something went wrong, restart
//			FCTL1 = 0;	//Restart 
//		}
//	}
//	while(1)	//Get software data
//	{
//		HandleWdt();
//		if (checkResponseMessage())
//		{
//			i=5;    //Skip >RTFW	
//			if(checkRemoteUppdateCrc())	//Check the checksum
//			{
//				HandleWdt();
//				P5OUT|=0x02;
//				systemtime=0;
//				if(recDsubBuf[i] == Hardware)	//Sending for right Hardware?
//				{
//					HandleWdt();
//					temp = recDsubBuf[++i]<<8;	//Msb of version
//					temp +=recDsubBuf[++i];		//Lsb of version
//					if(temp == newFirmwareVersion)	//Sending the right firmwareversion?
//					{
//						if(recDsubBuf[++i] == memorySpacePlacement)	//Right memoryplacement?
//						{
//							HandleWdt();
//							temp = recDsubBuf[++i];	//MSB of segment row
//							temp = temp<<24;
//							temp += recDsubBuf[++i];
//							temp = temp<<16;
//							temp += recDsubBuf[++i];
//							temp = temp<<8;
//							temp += recDsubBuf[++i]; //Lsb of segment row
//							if(temp == segmentNr)			//Right segment row?
//							{
//   								MPY = 100;							//Calculate the amount of programdata in message. Is i ASCII
//    							OP2 = (recDsubBuf[++i]&~(0x30));	
//    							j = RESLO;
//    							MPY = 10;      				
// 								OP2 = (recDsubBuf[++i]&~(0x30));	
// 	   							j += RESLO;
//  								j += (recDsubBuf[++i]&~(0x30));		//Variable J holds the amount of data	  							
//								temp = recDsubBuf[++i];				//MSB of address
//								temp = temp<<24;
//								temp += recDsubBuf[++i];
//								temp = temp<<16;
//								temp += recDsubBuf[++i];
//								temp = temp<<8;
//								temp += recDsubBuf[++i];				//LSB of address
//								
//								if(Flash_pnt == (unsigned int*) temp)		//Right address?
//								{
//									i++;	//Skip '0'	
//									if(recDsubBuf[++i] == '1')	//Last message to download?
//									{
//										moreDataToDownload = 0;
//									}
//									else
//									{
//										moreDataToDownload = 1;
//									}
//									_DINT();
//									HandleWdt();
//									FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
//									FCTL3 = FWKEY;                  // Clear Lock bit
//									for(k=0; k<j; k=k+2)			//Write the programdata
//									{
//  										temp=recDsubBuf[i+2]<<8;
//  										temp += recDsubBuf[++i];
//  										i++;
//    									*Flash_pnt++ = temp;        // Write value to flash 
//    									crc ^= temp;    			// Add the cecksum of data. Is used later for ProgramVerify   			
//  									}
// 	 								FCTL1 = FWKEY;            		// Clear WRT bit
//  									FCTL3 = FWKEY + LOCK;     		// Set LOCK bit
//  									flushRecDsubBuf();
//  									_EINT();						//enable interrupt
//  									segmentNr++;					//Count upp what segment we want to download next
//  									HandleWdt();	
//									P5OUT&=~0x02;
//								}
//								else
//								{
//									//Flash address was wrong. May be interrupt vector
//									if(temp == 0xFFE0) //Interruptaddress?
//									{
//										Interrupt_pnt = &(*(unsigned int*)Flash_pnt); 	//Address where the interruptvector is saved.
//										i++;	//Skip '0'	
//										if(recDsubBuf[++i] == '1')	//Last message to download?
//										{
//											moreDataToDownload = 0;
//										}
//										else
//										{
//											moreDataToDownload = 1;
//										}
//										_DINT();
//										HandleWdt();
//										FCTL1 = FWKEY + WRT;            // Set WRT bit for write operation
//										FCTL3 = FWKEY;                  // Clear Lock bit
//										for(k=0; k<j; k=k+2)			//Write the interruptvector
//										{
//  											temp=recDsubBuf[i+2]<<8;
//  											temp += recDsubBuf[++i];
//  											i++;
//    										*Flash_pnt++ = temp;        // Write value to flash        			
//  											crc ^= temp;
//  										}
//  										FCTL1 = FWKEY;            // Clear WRT bit
//  										FCTL3 = FWKEY + LOCK;     // Set LOCK bit	
//  										HandleWdt();	
//  										if(segmentNr == numberOfSegments)	//Have we downloaded all the calculated segments?
//  										{
//  											//Uppdate resetvector and reboot
//  											if(programVerify(crc, memorySpacePlacement, fileSize)) //Check the programdata
//  											{
//  												HandleWdt();
//  												Flash_pnt = (unsigned int *) 0xFE00;	
//  												*Flash_pnt++;						//Skip active sector
// 	 											temp = *Flash_pnt++;				//Save interruptA storage address
//  												temp = temp<<16;
//  												temp += *Flash_pnt;					//Save interruptB storage Address
//												FCTL1 = FWKEY + ERASE;          	// Set Erase bit
//	  											FCTL3 = FWKEY;                  	// Clear Lock bit
//  												*Flash_pnt = 0;						//Erase interruptsector
//  												FCTL1 = FWKEY + WRT;            	// Set WRT bit for write operation
//  												Flash_pnt = (unsigned int *) 0xFE00;
//  												if(memorySpacePlacement == 'A')
//  												{
//  													*Flash_pnt++ = 0x4141;			//set to AA
//  													*Flash_pnt++ = (unsigned int)&Interrupt_pnt;	//Address to InterruptA	storage
//  													*Flash_pnt++ = temp;							//Address to InterruptB storage		
//  												}
//  												else
//  												{
//  													*Flash_pnt++ = 0x4242;			//set to BB	
//  													*Flash_pnt++ = temp>>16;							//Address to InterruptA storage
//  													*Flash_pnt++ = (unsigned int)Interrupt_pnt; 	//Address to interruptB storage	
//  												}
//												Flash_pnt = (unsigned int *) 0xFFE0;	//Start address of interruptvectors		
//    											for (k=0; k<32; k=k+2)
//  												{
//    												*Flash_pnt++ = *Interrupt_pnt++;    //Write the new interruptvecktors  			
//  												}
//  												P5OUT|=0x04;
//  												for(i=0;i<70;i++)
//  												{
//  													time_delay(65535);
//  												}
//  												P5OUT&=~0x04;
//  												FCTL1 = FWKEY;                            // Clear WRT bit
//  												FCTL3 = FWKEY + LOCK;                     // Set LOCK bit
//  												FCTL1 = 0;								//Restart 
//  											}
//  											else
//  											{
//  												FCTL1 = 0;//Restart. Verify incorrect
//  											}
//  										}
//  										else
//  										{
//  											FCTL1 = 0;//Something went wrong. This should be the last segmentrow!	
//  										}
//									}	
//									else
//									{
//										//Not interruptvector address. Restart
//										FCTL1 = 0;	//Restart 
//									}	
//								}				
//							}
//							else
//							{
//								//Segmentrow was wrong. 
//								FCTL1 = 0;	//Restart 	
//							}
//						}
//						else
//						{
//							systemtime=1000;	//Memory placement was wrong (A || B)		
//						}
//					}
//					else
//					{
//						systemtime=1000; //Firmware was wrong	
//					}
//				}
//				else
//				{
//					systemtime=1000;	//Hardware was wrong
//				}
//			}
//			else
//			{
//				//Crc was wrong
//				if(numberOfRetries<5)
//				{
//					numberOfRetries++;
//					createQueryMessage(Hardware, newFirmwareVersion, memorySpacePlacement, segmentNr,  !remoteUppdateFromDsub);
//				}
//				else
//				{
//					//restart cpu
//					FCTL1 = 0;	//Restart 
//				}	
//			}	
//		}
//		if(systemtime >=6000) //1 min 
//		{
//			//Something went wrong, restart
//			FCTL1 = 0;	//Restart 
//		}
//		if(moreDataToDownload)
//		{
//			createQueryMessage(Hardware, newFirmwareVersion, memorySpacePlacement, segmentNr, 0);
//			moreDataToDownload=0;	
//		}	
//	}
//}
//
///**
// * Kontrollerar om det finns data i dsubbuffern
// * @returns 0 om det inte finns data 1 om det finns data.
// */
//short int dsubKbhit(void)
//{
//	return(startDsubPnt != endDsubPnt);
//}
//
///**
// * Rensar dsubbuffern
// */

//
//
