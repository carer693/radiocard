#include <msp430x24x.h>
#include "init.h"
#include "definitions.h"



void write_calibration_data_settings(unsigned char appData[])
	{
		
		
		char* ptr;
			int i;
			
			unsigned char j=0;
			HandleWdt();
			_DINT();																			// Disable interrupts
			IE1 &= ~(NMIIE);
			FlashClr( (int *) INFOC_MEM );				// erase segment C
			ptr = (char *) INFOC_MEM;					// Set pointer
			HandleWdt();
			for (i=0;i<19;i++) 
			{
				FlashWb( ptr++, appData[j+i]);
			}
			_EINT();
			IE1 |= NMIIE;																// Enable interrupt			
			
		
			//updatestate = 11;
			//updatemode = 20;															// 2 sek till f�r att hantera fler kommandon
		
	}

void init()
{
	unsigned int test=0;
	unsigned int i=0;
	unsigned int a=0;
	//unsigned char persist_write[8]={0x7E,0x00,0x04,0x08,0x52,0x57,0x52,0xFC};
	unsigned char calibData[19];
// WDTCTL = WDTPW+WDTHOLD;                 // Disable watchdog  
// Watchdog setup
initClk();
for(i=0;i<16;i++)
{
	calibData[i]=0;
}
WDTCTL = WDTPW + WDTHOLD + WDTSSEL; // ACLK clocksource 12kHz and counts up to 32768

initTimer();
initUart();



WDTCTL = WDTPW  + WDTSSEL;		// Start wdt
//UART0 initializing                   	
 

	HandleWdt();
 //Pin initializing
  P1DIR |=0x07;						// Set P1.0,1,2 to outputs
  P2DIR|=0x04;
  P2IE |= 0x02;						// Enable interrupt on P2.1 
  P2IES &= ~0x02;					// L-->H sensitiv
  P3SEL |= 0x30;                    // P3.4,5 = USART0 TXD/RXD
 // P3SEL |= 0x06;                    // Assign I2C pins to USCI_B0
  P4DIR |=0x02;						// Set P4.1 to digital output
  P6SEL |= 0x11;                    // Enable A/D channel A0 and A4
  P5DIR |= 0x06;                    // Set P5 to digital output
  P6DIR|=0x20;						// Set P6.5 to output
  P4OUT = 0x00;
  P5OUT = 0x00;
  P1OUT = 0x00;
  P6OUT &=~0x20;
  P2OUT &= ~0x04;
  
  //P6OUT|=0x20;						// Enable GND for imp.sensor
  
 

  //I2C initializing
  UCB0CTL1 |= UCSWRST;                      // Set SW reset
  UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;     // I2C Master, synchronous mode
  UCB0CTL1 = UCSSEL_2 + UCSWRST;            // Use SMCLK, keep SW reset
  UCB0BR0 = 10;                            	// fSCL = SMCLK/12 = ~100kHz
  UCB0BR1 = 0;

 


  for(i=0;i<400;i++)
	{
		impedance_data_to_calculate[i]=0;
	}
	for(i=0;i<20;i++)
  	{
  		receive[i]=0;
  	}
  initXbee();	
		
	
  
  	
  	///transparent_API_on();
  	//AT_prog(SIXTY_SEC,WAKETIME);
  	
  	
  	//OBS!! ALLT NEDAN I INIT() ENDAST I F�RSTA VERSIONEN
  
  	//Remember to change the CALIB definition when the device is calibrated
  	//OBS CHANGE SIGN FOR OFFSET IN RoadTempCalc()
  	//SIGN +//
  	calibData[0]=1;		// Calibration data for BelowZeroDerivata*100
  	calibData[1]=2;		// Calibration data for AboveZeroDerivata*100
  	calibData[2]=3;		// Calibration data for BelowZeroOffset*100
  	calibData[3]=4;		// Calibration data for AboveZeroOffset*100
  
  	
  	
  	//Ex:1,3461x^2+36,888*x^-37
  	//     A2        A1      A0
  	//S10A
  	/*calibData[4]=0x00;		// Calibration data for A2 High *10000
  	calibData[5]=0x70;
  	calibData[6]=0x16;		// Calibration data for A2 Low
  	calibData[7]=0x00;		// Calibration data for A1 High *1000
  	calibData[8]=0x7B;
  	calibData[9]=0x57;		// Calibration data for A1 Low
  	calibData[10]=0x03;		// Calibration data for A0 High *100
  	calibData[11]=0xF0;
  	calibData[12]=0x34;		// Calibration data for A0 Low
  	calibData[13]=0x00;		// Calibration data for B1 High *1000
  	calibData[14]=0xCA;		// 
  	calibData[15]=0xE9;		// Calibration data for B1 Low
  	calibData[16]=0x04;		// Calibration data for B0 High *100
  	calibData[17]=0x93;		// 
  	calibData[18]=0x7C;		// Calibration data for B0 Low
  	*/
  	/*calibData[4]=0x00;		// Calibration data for A2 High *10000
  	calibData[5]=0x70;
  	calibData[6]=0x16;		// Calibration data for A2 Low
  	calibData[7]=0x00;		// Calibration data for A1 High *1000
  	calibData[8]=0x7B;
  	calibData[9]=0x57;		// Calibration data for A1 Low
  	calibData[10]=0x03;		// Calibration data for A0 High *100
  	calibData[11]=0xF0;
  	calibData[12]=0x34;		// Calibration data for A0 Low
  	calibData[13]=0x00;		// Calibration data for B1 High *1000
  	calibData[14]=0xCA;		// 
  	calibData[15]=0xE9;		// Calibration data for B1 Low
  	calibData[16]=0x04;		// Calibration data for B0 High *100
  	calibData[17]=0x93;		// 
  	calibData[18]=0x7C;		// Calibration data for B0 Low*/
  	calibData[4]=0x05;		// Calibration data for A2 High *10000
  	calibData[5]=0x75;
  	calibData[6]=0x72;		// Calibration data for A2 Low
  	
  	calibData[7]=0x00;		// Calibration data for A1 High *1000
  	calibData[8]=0x20;
  	calibData[9]=0x91;		// Calibration data for A1 Low
  	
  	calibData[10]=0x00;		// Calibration data for A0 High *100
  	calibData[11]=0x6F;
  	calibData[12]=0x72;		// Calibration data for A0 Low
  	
  	calibData[13]=0x00;		// Calibration data for B1 High *1000
  	calibData[14]=0xA3;		// 
  	calibData[15]=0x6C;		// Calibration data for B1 Low
  	
  	calibData[16]=0x04;		// Calibration data for B0 High *100
  	calibData[17]=0x71;		// 
  	calibData[18]=0x1C;		// Calibration data for B0 Low*/
    test= BelowZeroDerivata;
	write_calibration_data_settings(calibData);
	test= BelowZeroDerivata;
	_NOP();
	/////////////////////////////////
	if(SENSORTYPE==0x11||SENSORTYPE==0x14||BACKUPPT100){
		test=ACTIVE_PT100;
		if(ACTIVE_PT100==PRIMARY||ACTIVE_PT100==0xFF){
			ActiveTempMeas=PRIMARY;
			DeepTemperature=32767;
		}
		else if(ACTIVE_PT100==BACKUP){
			ActiveTempMeas=BACKUP;
			SurfaceTemeprature=32767;
		}
		else{
			ActiveTempMeas=PRIMARY;
			DeepTemperature=32767;
		}
	}
	_NOP();
}

void initClk(){
	//MCLK initializing for 1MHz
  BCSCTL1 = CALBC1_1MHZ; 					// Set the freq. of the DCO to 1MHz
  DCOCTL = CALDCO_1MHZ; 				 
  
//VLOCLK initializing for ACLK 
BCSCTL3|=LFXT1S_2;					// Using VLOCLK for ACLK
}

void initUart(){
	 UCA0CTL1 |= UCSSEL_2;                     // SMCLK
  UCA0BR0 = 6;                              // 1MHz 9600
  UCA0BR1 = 0;                              // 1MHz 9600
  UCA0MCTL = UCBRF3 + UCOS16;
  UCA0CTL1 &= ~UCSWRST;                     // Initialize USART state machine
  
     //UART1 initializing                   	
  UCA1CTL1 |= UCSSEL_2;                     // SMCLK
  UCA1BR0 = 6;                              // 1MHz 9600
  UCA1BR1 = 0;                              // 1MHz 9600
  UCA1MCTL = UCBRF3 + UCOS16;
  UCA1CTL1 &= ~UCSWRST;                     // Initialize USART state machine
}

void initTimer(){
	 // KOLLA REGISTER
	TBCTL =  MC_1|ID_0|TBSSEL_2|TBCLR;
	//TBCTL = TBSSEL1 + TBCLR;// + TBIE;		// SMCLK, Clear, interrupt overflow enable
//TBCCTL0 = CCIE;										// Interrupt enable - System time
	TBCCR0 = 5000;
//	TBCCTL1 |= CCIE;								// Interrupt disable
	//TBCTL |= MC1;									// Continuous mode
	TBCCTL0 &= ~CCIFG;
}
void initXbee(){
		unsigned char a=0;
		unsigned char i=0;
		P1OUT|=0x04;
  
  	time_delay(65535);
  	P1OUT&=~0x04;
  	if(receive_XBee(6,4,0x8A)==0xFF){
  		API_mode=0;
  		for(i=0;i<5;i++){
  			if(transparent_API_on()==1){
  				break;}
  		}
  		if(i==5){
  			FCTL1=0; // Restart
  		}
  	}
  	API_mode=1;	
  	HandleWdt();
  	for(i=0;i<5;i++){
  	a=AT_prog(SET_BIT_ONE_THREE,SLEEPOPTION);
  	a+=AT_prog(TWO_SEC_S,SLEEPTIME);
  	a+=AT_prog(ONE_SEC,WAKEDELAY);
  	//a+=AT_prog(TRETTI_S_W,WAKETIME);
  	a+=AT_prog(ELEVEN_MIN_W,WAKETIME);
  	a+=AT_prog(NR_HOPS,NETWORKHOPS);
  	a+=AT_prog((FOUR),Digi0);	
  	a+=AT_prog((FOUR),Digi11);	
  	a+=AT_prog(GT_TIME,GUARDTIME);
  	a+=AT_prog(ND_TIME,NDTIMEOUT);
  	a+=AT_prog(OPCHE,CHANNEL);
  	a+=AT_prog(EIGHT,SLEEPMODE);
  	a+=AT_prog(ZERO,WRITE);
  	if(a==0){
  		break;}
  	}
  	
  	if(i>=5){
  		FCTL1=0; // Restart
  	}
  	
  	
}

void HandleWdt()
{
	WDTCTL = WDTPW + WDTCNTCL + WDTSSEL; 
}

// Disable/enable the Wdt timer before and after LPM4
void WdtONOFF(unsigned char OnOff)
{
	if(OnOff==0)
	{
		WDTCTL = WDTPW + WDTHOLD + WDTSSEL;
	}
	else if(OnOff==1)
	{
		WDTCTL = WDTPW  + WDTSSEL;
	}
}
	
